from rest_framework import routers
from .views import StudentViewJson

router = routers.DefaultRouter()

router.register(r'^', StudentViewJson)

urlpatterns = router.urls
