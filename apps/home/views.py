from rest_framework import viewsets
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import TemplateView

from apps.core.models import University
from apps.edudep.models import Student
from apps.edudep.serializers import StudentSerializer


class StudentViewJson(viewsets.ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class UniversityViewJson(viewsets.ModelViewSet):
    queryset = University.objects.all()
    serializer_class = StudentSerializer


class IndexView(LoginRequiredMixin, TemplateView):
    login_url = '/admin/login/'
    template_name = 'base.html'
