from .models import Country, Nationality
from .models import Region, City, District
from .models import Position, AcademicStatus, AcademicDegree
from .models import StaffStatus, Subject, Enrollment
from .models import LearningForm, LearningStatus

from rest_framework import viewsets, response, permissions

from .serializers import CountrySerializer, NationalitySerializer
from .serializers import RegionSerializer, CitySerializer, DistrictSerializer
from .serializers import PositionSerializer, AcademicStatusSerializer
from .serializers import AcademicDegreeSerializer, StaffStatusSerializer
from .serializers import SubjectSerializer, EnrollmentSerializer
from .serializers import LearningFormSerializer, LearningStatusSerializer


class CountryViewSet(viewsets.ModelViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(CountrySerializer(request.user,
                                                       context={'request': request}).data)
        return super(CountryViewSet, self).retrieve(request, pk)


class NationalityViewSet(viewsets.ModelViewSet):
    queryset = Nationality.objects.all()
    serializer_class = NationalitySerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(NationalitySerializer(request.user,
                                                           context={'request': request}).data)
        return super(NationalityViewSet, self).retrieve(request, pk)


class RegionViewSet(viewsets.ModelViewSet):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(RegionSerializer(request.user,
                                                      context={'request': request}).data)
        return super(RegionViewSet, self).retrieve(request, pk)


class CityViewSet(viewsets.ModelViewSet):
    queryset = City.objects.all()
    serializer_class = CitySerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(CitySerializer(request.user,
                                                    context={'request': request}).data)
        return super(CityViewSet, self).retrieve(request, pk)


class DistrictViewSet(viewsets.ModelViewSet):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(DistrictSerializer(request.user,
                                                        context={'request': request}).data)
        return super(DistrictViewSet, self).retrieve(request, pk)


class PositionViewSet(viewsets.ModelViewSet):
    queryset = Position.objects.all()
    serializer_class = PositionSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(PositionSerializer(request.user,
                                                        context={'request': request}).data)
        return super(PositionViewSet, self).retrieve(request, pk)


class AcademicStatusViewSet(viewsets.ModelViewSet):
    queryset = AcademicStatus.objects.all()
    serializer_class = AcademicStatusSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(AcademicStatusSerializer(request.user,
                                                              context={'request': request}).data)
        return super(AcademicStatusViewSet, self).retrieve(request, pk)


class AcademicDegreeViewSet(viewsets.ModelViewSet):
    queryset = AcademicDegree.objects.all()
    serializer_class = AcademicDegreeSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(AcademicDegreeSerializer(request.user,
                                                              context={'request': request}).data)
        return super(AcademicDegreeViewSet, self).retrieve(request, pk)


class StaffStatusViewSet(viewsets.ModelViewSet):
    queryset = StaffStatus.objects.all()
    serializer_class = StaffStatusSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(StaffStatusSerializer(request.user,
                                                           context={'request': request}).data)
        return super(StaffStatusViewSet, self).retrieve(request, pk)


class SubjectViewSet(viewsets.ModelViewSet):
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(SubjectSerializer(request.user,
                                                       context={'request': request}).data)
        return super(SubjectViewSet, self).retrieve(request, pk)


class EnrollmentViewSet(viewsets.ModelViewSet):
    queryset = Enrollment.objects.all()
    serializer_class = EnrollmentSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(EnrollmentSerializer(request.user,
                                                          context={'request': request}).data)
        return super(EnrollmentViewSet, self).retrieve(request, pk)


class LearningFormViewSet(viewsets.ModelViewSet):
    queryset = LearningForm.objects.all()
    serializer_class = LearningFormSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(LearningFormSerializer(request.user,
                                                            context={'request': request}).data)
        return super(LearningFormViewSet, self).retrieve(request, pk)


class LearningStatusViewSet(viewsets.ModelViewSet):
    queryset = LearningStatus.objects.all()
    serializer_class = LearningStatusSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(LearningStatusSerializer(request.user,
                                                              context={'request': request}).data)
        return super(LearningStatusViewSet, self).retrieve(request, pk)
