from django.contrib import admin
from .models import Country, Nationality, Region, City, District
from .models import Position, AcademicStatus, AcademicDegree, StaffStatus
from .models import Subject, Enrollment, LearningForm, LearningStatus
from .models import ComputerExperience, Languages, Ort


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(Nationality)
class NationalityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'country')
    list_filter = ('name', 'country')
    list_editable = ('name', 'country',)


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'country')
    list_filter = ('name', 'country',)
    list_editable = ('name', 'country',)


@admin.register(District)
class DistrictAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'region')
    list_filter = ('name', 'region',)
    list_editable = ('name', 'region',)


@admin.register(Position)
class NationalityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(AcademicStatus)
class AcStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(AcademicDegree)
class AcDegreeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'short')
    list_editable = ('name', 'short',)


@admin.register(StaffStatus)
class StaffStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(Subject)
class SubjectAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'coeff')
    list_editable = ('name', 'coeff')


@admin.register(Enrollment)
class EnrollmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(LearningForm)
class LearningFormAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(LearningStatus)
class LearningStatusAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(ComputerExperience)
class CompExperienceAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(Languages)
class LanguagesAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(Ort)
class OrtAdmin(admin.ModelAdmin):
    list_display = ('code', 'color', 'main_point', 'subject_point',)
    list_filter = ('color',)
