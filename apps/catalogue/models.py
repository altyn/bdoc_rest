from django.db import models
from django.utils.translation import ugettext_lazy as _


# LANGUAGE_LEVEL
LANGUAGE_LEVEL = (
    (
        ('Beginners',
         (
             ('A0', 'Never used'),
             ('A1', 'A1 – Beginner'),
             ('A1', 'A2 – Elementary'),
         )
         ),
        ('Independent',
         (
             ('B1', 'B1 – Intermediate'),
             ('B2', 'B2 – Upper-Intermediate'),
         )
         ),
        ('Proficient',
         (
             ('C1', 'C1 – Advanced'),
             ('C2', 'C2 – Upper-Advanced или Proficiency'),
         )
         )
    )
)

# CONTROL_TYPE
EXAM = 0
CREDIT = 1
COURSEWORK = 2
PRACTICE = 3
CONTROL_TYPE = (
    (EXAM, 'Экзамен'),
    (CREDIT, 'Зачет'),
    (COURSEWORK, 'Курсовая'),
    (PRACTICE, 'Практика'),
)

# SEX_CHOISES
MALE = 0
FEMALE = 1
SEX_CHOISES = (
    (MALE, "Мужчина"),
    (FEMALE, "Женщина"),
)

# MARKS_CHOISES
FAILED = 0
SATISFACTORY = 1
GOOD = 2
EXCELLENT = 3
PASSED = 4
DID_NOT_PASSED = 5
NOT_DEFINED = 6
MARKS_CHOISES = (
    (FAILED, "Не удов."),
    (SATISFACTORY, "Удов."),
    (GOOD, "Хорошо"),
    (EXCELLENT, "Отлично"),
    (PASSED, "Зачтено"),
    (DID_NOT_PASSED, "Не зачтено"),
    (NOT_DEFINED, "Ничего"),
)


# MARITIAL STATUS
SINGLE = 0
MARRIED = 1
DIVORCED = 2
A_WIDOWER = 3
MARITIAL_STATUS = (
    (SINGLE, "Холост/Не замужем"),
    (MARRIED, "Замужем/Женат"),
    (DIVORCED, "Разведен(а)"),
    (A_WIDOWER, "Вдовец/Вдова")
)

# ORT_CERTIFICATE_TEST_COLOR
DEFAULT_COLOR = '#FFFFFF'
GOLD = '#FFD700'
PURPLE = '#800080'
YELLOW = '#FFFF00'
BLUE = '#0000FF'
RED = '#FF0000'
ROSE = '#FFE4E1'
ORT_CERT_COLOR = (
    (DEFAULT_COLOR, 'Без цвета'),
    (GOLD, 'Золотой'),
    (PURPLE, 'Фиолетовый'),
    (YELLOW, 'Желтый'),
    (BLUE, 'Голубой'),
    (RED, 'Красный'),
    (ROSE, 'Розовый'),
)


# Страны
class Country(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))

    class Meta:
        verbose_name = _('country')
        verbose_name_plural = _('countries')

    def __str__(self):
        return '%s' % self.name


# Национальности
class Nationality(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))

    class Meta:
        verbose_name = _('nationality')
        verbose_name_plural = _('nationalities')
        ordering = ['id']

    def __str__(self):
        return '%s' % self.name


# Области
class Region(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))
    country = models.ForeignKey(Country, verbose_name=_('country'), default=0)

    class Meta:
        verbose_name = _('region_district')
        verbose_name_plural = _('regions_districts')

    def __str__(self):
        return '%s' % self.name


# Районные центры и города
class City(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('city'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))
    country = models.ForeignKey(Country, verbose_name=_('country'), default=0)

    class Meta:
        verbose_name = _('city')
        verbose_name_plural = _('cities')

    def __str__(self):
        return '%s' % self.name


# Районы
class District(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))
    region = models.ForeignKey(Region, verbose_name=_('region'), default=0)

    class Meta:
        verbose_name = _('district')
        verbose_name_plural = _('districts')

    def __str__(self):
        return '%s' % self.name


# Должность сотрудника
class Position(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))

    class Meta:
        verbose_name = _('position')
        verbose_name_plural = _('positions')

    def __str__(self):
        return '%s' % self.name


# Ученые звания
class AcademicStatus(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))

    class Meta:
        verbose_name = _('academic_status')
        verbose_name_plural = _('academic_statuses')

    def __str__(self):
        return '%s' % self.name


# Ученые степени
class AcademicDegree(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))
    short = models.CharField(max_length=10,  verbose_name=_('short'))

    class Meta:
        verbose_name = _('academic_degree')
        verbose_name_plural = _('academic_degrees')

    def __str__(self):
        return '%s' % self.name


# Статус ППС
class StaffStatus(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100,  verbose_name=_('name_of_smth'))

    class Meta:
        verbose_name = _('staff_status')
        verbose_name_plural = _('staff_status')

    def __str__(self):
        return '%s' % self.name


# Дисциплины
class Subject(models.Model):
    id = models.CharField(primary_key=True, auto_created=False, max_length=10, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))
    coeff = models.PositiveIntegerField(verbose_name=_('coeff'), default=1)

    class Meta:
        verbose_name = _('subject')
        verbose_name_plural = _('subjects')
        ordering = ['name']

    def __str__(self):
        return '%s' % self.name


# Условия зачисления
class Enrollment(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))

    class Meta:
        verbose_name = _('enrollment_condition')
        verbose_name_plural = _('enrollment_conditions')

    def __str__(self):
        return '%s' % self.name


# Форма обучения
class LearningForm(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100,  verbose_name=_('name_of_smth'))

    class Meta:
        verbose_name = _('learning_form')
        verbose_name_plural = _('learning_forms')

    def __str__(self):
        return '%s' % self.name


# Состояние обучения
class LearningStatus(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))

    class Meta:
        verbose_name = _('learning_status')
        verbose_name_plural = _('learning_status')

    def __str__(self):
        return '%s' % self.name


# Компьютерные навыки
class ComputerExperience(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))

    class Meta:
        verbose_name = _('comp_experience')
        verbose_name_plural = _('comp_experiences')

    def __str__(self):
        return '%s' % self.name


# Знание языки
class Languages(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    name = models.CharField(max_length=100, verbose_name=_('name_of_smth'))

    class Meta:
        verbose_name = _('language_skills')
        verbose_name_plural = _('language_skills')

    def __str__(self):
        return '%s' % self.name


# ОРТ ЦООМО
class Ort(models.Model):
    #student = models.ForeignKey()
    code = models.PositiveIntegerField()
    color = models.PositiveIntegerField()
    main_point = models.PositiveIntegerField()
    subject_point = models.PositiveIntegerField()

    class Meta:
        verbose_name = _('ORT')
        verbose_name_plural = _('ORT')

    def __str__(self):
        return '%s' % self.code
