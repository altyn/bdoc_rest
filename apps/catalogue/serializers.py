from rest_framework import serializers

from .models import Country, Nationality
from .models import Region, City, District
from .models import Position, AcademicStatus, AcademicDegree
from .models import StaffStatus, Subject, Enrollment
from .models import LearningForm, LearningStatus


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'


class NationalitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Nationality
        fields = '__all__'


class RegionSerializer(serializers.ModelSerializer):
    country = serializers.StringRelatedField()

    class Meta:
        model = Region
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields = '__all__'


class PositionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Position
        fields = '__all__'


class AcademicStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = AcademicStatus
        fields = '__all__'


class AcademicDegreeSerializer(serializers.ModelSerializer):
    class Meta:
        model = AcademicDegree
        fields = '__all__'


class StaffStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = StaffStatus
        fields = '__all__'


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subject
        fields = '__all__'


class EnrollmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enrollment
        fields = '__all__'


class LearningFormSerializer(serializers.ModelSerializer):
    class Meta:
        model = LearningForm
        fields = '__all__'


class LearningStatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = LearningStatus
        fields = '__all__'
