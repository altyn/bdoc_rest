from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from apps.core.models import Department
from apps.catalogue.models import Position, AcademicStatus, AcademicDegree
from apps.catalogue.models import StaffStatus, Nationality, SEX_CHOISES, FEMALE
from apps.catalogue.models import Country, District, Region, City


WAITING = 0
PENDING = 1
FINISHED = 2
STATUS_CHOISES = (
    (WAITING, 'Waiting'),
    (PENDING, 'Pending'),
    (FINISHED, 'Finished'),
)


# Create your models here.
class Staff(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=False, verbose_name=_('id'))
    last_name = models.CharField(max_length=100, verbose_name=_('first_name'))
    name = models.CharField(max_length=100, verbose_name=_('name'))
    middle_name = models.CharField(max_length=100, blank=True, verbose_name=_('last_name'))
    profile_img = models.FileField(upload_to='images/staff/',
                                   default='images/staff/profile.png',  verbose_name=_('profile_img'))
    birthdate = models.DateField(null=False, default="1980-01-01", verbose_name=_('birthday'))
    position = models.ForeignKey(Position, default=0, verbose_name=_('position'))
    academic_status = models.ForeignKey(AcademicStatus, default=0, verbose_name=_('academic_status'))
    academic_degree = models.ForeignKey(AcademicDegree, default=0, verbose_name=_('academic_degree'))
    staff_status = models.ForeignKey(StaffStatus, default=0,  verbose_name=_('staff_status'))
    academic_load = models.IntegerField(default=0, null=False, verbose_name=_('academic_load'))
    nationality = models.ForeignKey(Nationality, default=0, verbose_name=_('nationality'))
    mother_tongue = models.CharField(max_length=100, default="Русский", verbose_name=_('mother_tongue'))
    citizenship = models.ForeignKey(Country, default=0, verbose_name=_('citizenship'))
    sex = models.IntegerField(choices=SEX_CHOISES, default=FEMALE, verbose_name=_('sex'))
    email = models.EmailField(default='no@email.home', blank=True, verbose_name=_('email'))
    phone = models.CharField(max_length=15, blank=True, verbose_name=_('phone'))
    department = models.ForeignKey(Department, verbose_name=_('department'))
    quit = models.BooleanField(default=False, verbose_name=_('quit'))
    quit_date = models.DateField(default=timezone.now, verbose_name=_('quit_date'), null=True, blank=True)
    edited = models.DateTimeField(default=timezone.now, verbose_name=_('edited'), editable=False)

    class Meta:
        verbose_name = _('staff')
        verbose_name_plural = _('staff')

    def __str__(self):
        return '%s %s %s' % (self.last_name, self.name, self.middle_name)


class StaffAddress(models.Model):
    staff = models.ForeignKey(Staff, verbose_name=_('staff'))
    country = models.ForeignKey(Country, verbose_name=_('country'), default=0)
    region = models.ForeignKey(Region, verbose_name=_('region'), default=0)
    district = models.ForeignKey(District, verbose_name=_('district'), default=0)
    city = models.ForeignKey(City, verbose_name=_('city'), default=0)
    address = models.CharField(verbose_name=_('address'), max_length=300)
    permanent = models.BooleanField( verbose_name=_('permanent'), default=True)

    class Meta:
        verbose_name = _('staff_address')
        verbose_name_plural = _('staff_addresses')

    def __str__(self):
        return '%s' % self.address
