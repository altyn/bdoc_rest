from django.apps import AppConfig


class HrConfig(AppConfig):
    name = 'apps.hr'
    verbose_name = 'Отдел кадров'
