from django.contrib import admin
from .models import Staff, StaffAddress
from apps.cv.models import AdditionalInf, Conferences, Education
from apps.cv.models import LanguageKnowledge, Publications, Seminars
from apps.cv.models import Work, Projects
from jet.admin import CompactInline
from jet.filters import RelatedFieldAjaxListFilter


# Register your models here.
class StaffAddressInline(CompactInline):
    model = StaffAddress
    extra = 0
    show_change_link = True


class EducationInline(CompactInline):
    model = Education
    extra = 0


class WorkInline(CompactInline):
    model = Work
    extra = 0


class SeminarsInline(CompactInline):
    model = Seminars
    extra = 0


class PublicationsInline(CompactInline):
    model = Publications
    extra = 0


class ConferencesInline(CompactInline):
    model = Conferences
    extra = 0


class LanguageKnowledgeInline(CompactInline):
    model = LanguageKnowledge
    extra = 0


class AdditionalInformationInline(CompactInline):
    model = AdditionalInf
    extra = 0


class ProjectsInline(CompactInline):
    model = Projects
    extra = 0


@admin.register(Staff)
class StaffAdmin(admin.ModelAdmin):
    inlines = [StaffAddressInline, EducationInline,
               WorkInline, ConferencesInline, ProjectsInline,
               SeminarsInline, PublicationsInline,
               LanguageKnowledgeInline, AdditionalInformationInline, ]
    list_display = ('id', 'last_name', 'name', 'middle_name', 'sex', 'position',)
