from django.apps import AppConfig


class EdudepConfig(AppConfig):
    name = 'apps.edudep'
    verbose_name = 'Учебный отдел'