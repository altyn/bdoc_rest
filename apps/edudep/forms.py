from django import forms
from django.utils import timezone

# Foreign Keys
from apps.core.models import Groups, MarksSettings
from apps.edudep.models import UchPlanStudents
from apps.catalogue.models import SEX_CHOISES, MARITIAL_STATUS
from .models import Student
from apps.catalogue.models import Subject

from django.utils.translation import ugettext_lazy as _
from apps.catalogue.models import Nationality, Country, Enrollment, LearningForm, LearningStatus


class StudentForm(forms.ModelForm):
    first_name = forms.CharField(max_length=100, label=_('first_name'))
    name = forms.CharField(max_length=100, label=_('name'))
    last_name = forms.CharField(label=_('last_name'), max_length=100)
    birthday = forms.DateField(label=_('birthday'))
    sex = forms.ChoiceField(choices=SEX_CHOISES, label=_('sex'))
    maritial_status = forms.ChoiceField(choices=MARITIAL_STATUS, label=_('maritial_status'))
    nationality = forms.ModelChoiceField(queryset=Nationality.objects.all(), label=_('nationality'))
    citizenship = forms.ModelChoiceField(queryset=Country.objects.all(), label=_('citizenship'))
    phone = forms.CharField(label=_('phone'), max_length=18)
    email = forms.CharField(label=_('email'), max_length=100)
    enrollment = forms.ModelChoiceField(queryset=Enrollment.objects.all(), label=_('enrollment'))
    learning_status = forms.ModelChoiceField(queryset=LearningStatus.objects.all(), label=_('learning_status'))
    learning_form = forms.ModelChoiceField(queryset=LearningForm.objects.all(), label=_('learning_form'))
    profile_img = forms.FileField(max_length=100, label=_('profile_img'))
    group_code = forms.ModelChoiceField(queryset=Groups.objects.all(), label=_('group_code'))
    course = forms.IntegerField(label=_('course'))
    semestr = forms.IntegerField(label=_('semestr'))
    contract_summ = forms.IntegerField(label=_('contract_summ'))

    class Meta:
        model = Student
        fields = ['first_name', 'name', 'last_name', 'birthday',
                    'sex', 'maritial_status', 'nationality', 'citizenship', 'phone',
                    'email', 'enrollment', 'learning_status', 'learning_form',
                    'profile_img', 'group_code', 'course', 'semestr', 'contract_summ']
        widgets = {
            'birthday': forms.DateInput(format='%d/%m/%Y'),
        }


class UchPlanStudentForm(forms.ModelForm):
    group_code = forms.ModelChoiceField(queryset=Groups.objects.all(), empty_label="(Nothing)")
    student = forms.ModelChoiceField(queryset=Student.objects.all(), empty_label="(Nothing)")
    semestr = forms.IntegerField(min_value=1)
    disc_code = forms.ModelChoiceField(queryset=Subject.objects.all(), empty_label="(Nothing)")
    clock = forms.FloatField(min_value=0.0)
    credit = forms.FloatField(min_value=0.0)
    ball = forms.IntegerField(min_value=0)
    ocenka = forms.CharField(max_length=8,)
    controlb = forms.ChoiceField(widget=forms.RadioSelect)
    begunok = forms.BooleanField(required=True)
    number_of_begunok = forms.CharField(max_length=30)
    number_of_vedomost = forms.CharField(max_length=30)
    date = forms.DateField(forms.SelectDateWidget)

    class Meta:
        model = UchPlanStudents
        fields = ['student', 'semestr', 'disc_code', 'first_module',
                  'second_module', 'writing', 'ocenka', 'number_of_vedomost', ]

    # def clean(self):
    #     sets = MarksSettings.objects.get(pk=1)
    #     allow_marks = sets.is_available()
    #     allow_dates = sets.is_date_available()
    #     if allow_marks and allow_dates:
    #         raise forms.ValidationError("Dates are incorrect")
    #     return self.cleaned_data
#         fields = ['student', 'semestr', 'disc_code', 'first_module',
#                   'second_module', 'writing', 'ocenka', 'number_of_vedomost']
#
#         def clean(self):
#             sets = MarksSettings.objects.get(pk=1)
#             allow_marks = sets.is_available()
#             allow_dates = sets.is_date_available()
#             if allow_marks and allow_dates:
#                 return self.cleaned_data
#             else:
#                 raise forms.ValidationError("Разрешить не разрешено")

    # group_code = forms.IntegerField(queryset=Groups.objects.all())
    # student = forms.ModelChoiceField(queryset=Student.objects.all())
    # semestr = forms.PositiveIntegerField(null=False, default=1)
    # disc_code = forms.ModelChoiceField(Subject, on_delete=models.DO_NOTHING, default=0)
    # clock = forms.FloatField(default=0)
    # credit = forms.FloatField(default=0)
    # ball = forms.PositiveIntegerField(default=0)
    # ocenka = forms.CharField(max_length=8, default=None)
    # controlb = forms.IntegerField(choices=CONTROL_TYPE, default=EXAM)
    # begunok = forms.BooleanField(default=False)
    # number_of_begunok = forms.CharField(max_length=30, default=0)
    # number_of_vedomost = forms.CharField(max_length=30, default=0)
    # date = forms.DateField(null=True, blank=True)
