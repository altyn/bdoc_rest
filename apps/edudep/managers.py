from django.db.models.manager import Manager


class UchPlManager(Manager):
    def create_plan(self, student, semestr, clock, credit, controlb, disc_code, group_code, uchplgroup_code):
        uchpl = self.create(student=student, semestr=semestr, clock=clock, credit=credit,
                            controlb=controlb, disc_code=disc_code, group_code=group_code,
                            uchplgroup_code=uchplgroup_code)
        return uchpl
