from apps.core.models import MarksSettings
from apps.catalogue.models import (EXCELLENT, GOOD, SATISFACTORY, FAILED,
                                   PASSED, DID_NOT_PASSED, NOT_DEFINED)
from apps.catalogue.models import EXAM, CREDIT, COURSEWORK, PRACTICE


sport = 'SPOR 101'
# marks_level = MarksSettings.objects.filter().first()
# excellent_end = marks_level.marks_high_end
# excellent_start = marks_level.marks_high_start
# good_end = marks_level.marks_well_end
# good_start = marks_level.marks_well_start
# satisfactory_end = marks_level.marks_satisfactorily_end
# satisfactory_start = marks_level.marks_satisfactorily_start
# bad_end = marks_level.marks_bad_end

marks_level = 0
excellent_end = 0
excellent_start = 0
good_end = 0
good_start = 0
satisfactory_end = 0
satisfactory_start = 0
bad_end = 0

"""
    :returns EXCELLENT, GOOD, SATISFACTORY, FAILED
    depends on marks
"""


def grades_for_exam(mark):
    if 0 < mark <= 200:
        divided = mark / 2
        if excellent_start <= divided <= excellent_end:
            return EXCELLENT
        elif good_start <= divided <= good_end:
            return GOOD
        elif satisfactory_start <= divided <= satisfactory_end:
            return SATISFACTORY
        elif 0 < divided <= bad_end:
            return FAILED
    else:
        return NOT_DEFINED


def grades_for_sport(mark):
    if satisfactory_start <= mark <= excellent_end:
        return PASSED
    elif 0 < mark < satisfactory_start:
        return DID_NOT_PASSED
    else:
        return NOT_DEFINED


def grades_for_credit(mark):
    if excellent_start <= mark <= excellent_end:
        return EXCELLENT
    elif good_start <= mark <= good_end:
        return GOOD
    elif satisfactory_start <= mark <= satisfactory_end:
        return SATISFACTORY
    elif 0 < mark < satisfactory_start:
        return DID_NOT_PASSED
    else:
        return NOT_DEFINED


"""
    This function
    :returns GRADES for COURSEWORK and PRACTICE
"""


def grades_for_cp(mark):
    if mark == 5:
        return EXCELLENT
    elif mark == 4:
        return GOOD
    elif mark == 3:
        return SATISFACTORY
    elif 0 <= mark <= 2:
        return FAILED
    else:
        return NOT_DEFINED


def is_sport(disc_code):
    if disc_code == "SPOR 101":
        return True
    return False


def return_grade(control, subject, mark):
    if control == CREDIT.numerator:
        if is_sport(subject):
            return grades_for_sport(mark)
        else:
            return grades_for_credit(mark)
    elif control == EXAM.numerator:
        return grades_for_exam(mark)
    elif control == PRACTICE.numerator or control == COURSEWORK.numerator:
        return grades_for_cp(mark)
    else:
        return NOT_DEFINED
