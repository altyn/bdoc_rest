from django.contrib import admin
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from .models import Student
from apps.catalogue.models import Subject
from .models import StudentAddress, StudentPassport, StudentAttestat
from .models import StudentDiploma, Prikaz, UchPlanStudents, UchPlanGroup
from .models import UchplanStudentsVodomosti
from .models import DiplomaFromUniversity, StudentAdditionalInf, StudentBirthPlace
from apps.core.models import Groups


class AddressInline(admin.StackedInline):
    model = StudentAddress
    extra = 0


class BirthplaceInline(admin.StackedInline):
    model = StudentBirthPlace
    extra = 0


class PassportInline(admin.StackedInline):
    model = StudentPassport
    extra = 0


class AttestatInline(admin.StackedInline):
    model = StudentAttestat
    extra = 0


class DiplomaInline(admin.StackedInline):
    model = StudentDiploma
    extra = 0


class PrikazInline(admin.StackedInline):
    model = Prikaz
    extra = 0


class DiplomaFromUniversityInline(admin.StackedInline):
    model = DiplomaFromUniversity
    extra = 0


class StudentAdditionalInline(admin.StackedInline):
    model = StudentAdditionalInf
    extra = 0


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    inlines = [AddressInline, PassportInline, BirthplaceInline, AttestatInline, DiplomaInline,
               DiplomaFromUniversityInline, StudentAdditionalInline, PrikazInline]
    list_display = ('id', 'first_name', 'name', 'last_name', 'group_code')
    list_filter = (
        'group_code',
    )
    search_fields = ('first_name', 'name', 'last_name')


@admin.register(UchPlanGroup)
class UchPlanGroupAdmin(admin.ModelAdmin):
    list_display = ('group', 'semestr', 'subject', 'teacher', 'clock', 'credit', 'control', 'print_button',)
    actions = ['raznoska']
    list_filter = ('group', 'semestr', 'teacher',)

    def print_button(self, obj):
        return format_html("<a type='submit' class='button' href='/reports/group/{}/current_v/print/', target='_blank'>"
                           "Тек. Ведомость</a> "
                           "<a type='submit' class='button' href='/reports/group/{}/v/print/', target='_blank'>"
                           "Ведомость</a> "
                           "<a type='submit' class='button' href='/reports/group/{}/journal/download/', "
                           "target='_blank'>Журнал баллов</a>",
                           obj.pk,
                           obj.pk,
                           obj.pk,
                           )

    print_button.short_description = _('print')
    print_button.allow_tags = True

    def raznoska(self, request, queryset):
        selected_ids = queryset.values('id', 'semestr', 'clock', 'credit', 'control', 'subject_id', 'group', )
        group_list = list(queryset.values_list('group', flat=True))
        students = Student.objects.filter(group_code_id=group_list[0])
        rows_updated = 0

        for stid in students:
            for s in selected_ids:
                semestr = s['semestr']
                clock = s['clock']
                credit = s['credit']
                controlb = s['control']
                disc_code = Subject.objects.get(id=s['subject_id'])
                group_code = Groups.objects.get(id=str(group_list[0]))
                uchplgroup_code = UchPlanGroup.objects.get(id=s['id'])

                plan = UchPlanStudents.uchpl_m.create_plan(stid, semestr, clock, credit,
                                                           controlb, disc_code, group_code,
                                                           uchplgroup_code)

                rows_updated += 1

        if rows_updated == 1:
            message_bit = "Было всего 1 план"
        else:
            message_bit = "%s учебных планов" % rows_updated
        self.message_user(request, "%s учебных планов успешно создано." % message_bit)

    raznoska.short_description = "Разноска учебного плана"
    admin.site.add_action(raznoska)


@admin.register(UchPlanStudents)
class UchPlanStudentsAdmin(admin.ModelAdmin):
    list_display = ('student', 'semestr', 'disc_code', 'first_module',
                    'second_module', 'writing', 'mark', 'number_of_vedomost', 'begunok_button')
    list_filter = ('group_code', 'disc_code', 'semestr',)

    def begunok_button(self, obj):
        return format_html("<a type='submit' class='button' href='/uchplans/students/{}/b/print/', target='_blank'>"
                           "Бегунок</a>"
                           "<a type='submit' class='button' href='/reports/student/{}/curr_uchplan/{}', target='_blank'>"
                           "Тек уч. план</a>",
                           obj.pk,
                           obj.student.id,
                           obj.semestr,
                           # self. request.GET.get('semestr')
                           )

    def mark(self, obj):
        if obj.get_ocenka_display() == "Ничего":
            return ""
        else:
            return obj.get_ocenka_display()

    begunok_button.short_description = _('print')
    begunok_button.allow_tags = True
    mark.short_description = _('mark')
    mark.allow_tags = True


@admin.register(UchplanStudentsVodomosti)
class GroupsVodemosti(admin.ModelAdmin):
    list_display = ('student', 'semestr', 'disc_code', 'first_module',
                    'second_module', 'writing', 'mark', 'number_of_vedomost')
    list_filter = ('group_code', 'disc_code', 'semestr',)
    list_editable = ('first_module', 'second_module', 'writing', 'number_of_vedomost',)

    def mark(self, obj):
        if obj.get_ocenka_display() == "Ничего":
            return ""
        else:
            return obj.get_ocenka_display()

    def has_add_permission(self, request):
        return False

    mark.short_description = _('mark')
    mark.allow_tags = True
