from rest_framework import viewsets, permissions, generics
from .serializers import (StudentSerializer, UchPlanGroupSerializer,
                          UchPlanStudentsListSerializer, UchPlanStudentsDetailSerializer)
from django.views.generic import ListView, DetailView, FormView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.utils import timezone

from .models import Student, UchPlanGroup, UchPlanStudents
from .forms import StudentForm


# Create your views here.
class StudentsIndex(LoginRequiredMixin, ListView):
    template_name = "edudep/students.html"
    model = Student
    paginate_by = 20
    queryset = Student.objects.all()
    context_object_name = "students_list"


class StudentsAPIView(generics.ListCreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class StudentsDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class StudentDetailViewSet(LoginRequiredMixin, DetailView):
    login_url = '/admin/login/'
    template_name = 'edudep/student_detail.html'
    queryset = Student.objects.select_related()
    context_object_name = 'student'


class StudentCreateViewSet(CreateView):
    model = Student
    template_name = 'edudep/new_student.html'
    fields = [
        'id', 'first_name', 'name', 'last_name', 'birthday',
        'sex', 'maritial_status', 'nationality', 'citizenship', 'phone',
        'email', 'enrollment', 'learning_status', 'learning_form',
        'profile_img', 'group_code', 'course', 'semestr', 'contract_summ'
    ]


class UchPlanGroupViewSet(viewsets.ModelViewSet):
    queryset = UchPlanGroup.objects.all()
    serializer_class = UchPlanGroupSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(UchPlanGroupSerializer(request.user,
                                                            context={'request': request}).data)
        return super(UchPlanGroupViewSet, self).retrieve(request, pk)


class UchPlanGroupIndex(LoginRequiredMixin, ListView):
    template_name = "edudep/uchplangroup.html"
    model = UchPlanGroup
    paginate_by = 12
    queryset = UchPlanGroup.objects.all()
    context_object_name = "uchplangroup_list"


class UchPlanGroupDetailView(LoginRequiredMixin, DetailView):
    login_url = '/admin/login/'
    template_name = 'edudep/current_vedom.html'
    queryset = UchPlanGroup.objects.select_related()
    context_object_name = 'uchplgr'


class UchPlanStudentsListAPIView(generics.ListCreateAPIView):
    queryset = UchPlanStudents.objects.all()
    serializer_class = UchPlanStudentsListSerializer


class UchPlanStudentDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve and Update UchPlanStudent
    Allowed request method: Get, Post
    """
    queryset = UchPlanStudents.objects.all()
    serializer_class = UchPlanStudentsDetailSerializer


# class UchPlanStudentViewSet(viewsets.ModelViewSet):
    # """
    # View to list all uchplans in the DB.
    #
    # * Requires basic authentication.
    # * Only staff users are able to access this view.
    # """
    # queryset = UchPlanStudents.objects.all()
    # serializer_class = UchPlanStudentsSerializer
    # # permission_classes = (permissions.IsAuthenticated,)
    #
    # @list_route(methods=['get'])
    # def show_all(self, request):
    #     stud_uchpls = UchPlanStudents.objects.all().order('-id')
    #     page = self.paginate_queryset(stud_uchpls)
    #     if page is not None:
    #         serializer = self.get_serializer(page, many=True)
    #         return self.get_paginated_response(serializer.data)
    #
    #     serializer = self.get_serializer(stud_uchpls, many=True)
    #     return Response(serializer.data)
    #
    #     # def retrieve(self, request, pk=None):
    #     #     if pk == 'i':
    #     #         return Response(UchPlanStudentsSerializer(request.user,
    #     #                                                            context={'request': request}).data)
    #     #     return super(UchPlanStudentViewSet, self).retrieve(request, pk)
    #
    # @detail_route(methods=['post'])
    # def update_marks(self, request, pk=None):
    #     vedomost_detail = self.get_object()
    #     serializer = UchPlanStudentsSerializer(data=request.data)
    #     if serializer.is_valid():
    #         # vedomost_detail.set_password(serializer.data['first_module'])
    #         vedomost_detail.save()
    #         return Response({'status': 'Updated'})
    #     else:
    #         return Response(serializer.errors,
    #                         status=status.HTTP_400_BAD_REQUEST)
    #     # serializer = UchPlanGroupSerializer(data=request.data)
    #     # if serializer.is_valid():
    #     #     serializer.save()
    #     #     return Response(serializer.data, status=status.HTTP_201_CREATED)
    #     # return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UchPlanStudentIndex(LoginRequiredMixin, ListView):
    template_name = "edudep/uchplanstudent.html"
    model = UchPlanStudents
    paginate_by = 20
    queryset = UchPlanStudents.objects.all()
    context_object_name = "uchplanstudent_list"


    # def post_update(request):
    #     if not request.user.is_staff or not request.user.is_superuser:
    #         raise Http404
    #
    #     form = UchPlanStudentForm(request.POST or None, request.FILES or None)
    #     if form.is_valid():
    #         instance = form.save(commit=False)
    #         instance.user = request.user
    #         # message success
    #         messages.success(request, "Success")
    #         return HttpResponseRedirect(instance.pk)
    #     context = {
    #         "form": form,
    #     }
    #     return render(request)



