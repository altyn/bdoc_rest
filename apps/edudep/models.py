from django.utils import timezone
from django.db import models
from apps.core.models import Groups
from apps.hr.models import Staff
from .managers import UchPlManager
import apps.edudep.utils as marks_class
from apps.core.models import MarksSettings as marks_settings

# Catalogue imports
from apps.catalogue.models import Nationality, Enrollment, SEX_CHOISES, FEMALE
from apps.catalogue.models import LearningStatus, LearningForm, Country
from apps.catalogue.models import Region, District, City, Subject
from apps.catalogue.models import CONTROL_TYPE, EXAM, MARKS_CHOISES, NOT_DEFINED, MARITIAL_STATUS, SINGLE

from django.utils.translation import ugettext_lazy as _


# Student Information.
class Student(models.Model):
    id = models.IntegerField(primary_key=True, verbose_name=_('id'), auto_created=False)
    first_name = models.CharField(max_length=100, verbose_name=_('first_name'), null=False)
    name = models.CharField(max_length=100, verbose_name=_('name'), null=False)
    last_name = models.CharField(verbose_name=_('last_name'), max_length=100)
    birthday = models.DateField(null=False, verbose_name=_('birthday'), default="1980-01-01")
    sex = models.IntegerField(choices=SEX_CHOISES, default=FEMALE, verbose_name=_('sex'))
    maritial_status = models.IntegerField(choices=MARITIAL_STATUS, default=SINGLE, verbose_name=_('maritial_status'))
    nationality = models.ForeignKey(Nationality, on_delete=models.DO_NOTHING, verbose_name=_('nationality'), default=0)
    citizenship = models.ForeignKey(Country, on_delete=models.DO_NOTHING, verbose_name=_('citizenship'), default=0)
    phone = models.CharField(verbose_name=_('phone'), max_length=18)
    email = models.CharField(verbose_name=_('email'), max_length=100)
    enrollment = models.ForeignKey(Enrollment, on_delete=models.DO_NOTHING, verbose_name=_('enrollment'), default=0)
    learning_status = models.ForeignKey(LearningStatus, on_delete=models.DO_NOTHING, verbose_name=_('learning_status'), default=0)
    learning_form = models.ForeignKey(LearningForm, verbose_name=_('learning_form'), default=0)
    profile_img = models.CharField(max_length=100, verbose_name=_('profile_img'), default='images/staff/profile.png')
    group_code = models.ForeignKey(Groups, verbose_name=_('group_code'), default=0)
    course = models.PositiveIntegerField(verbose_name=_('course'), default=1)
    semestr = models.PositiveIntegerField(verbose_name=_('semestr'), default=1)
    contract_summ = models.PositiveIntegerField(verbose_name=_('contract_summ'), default=0)
    created_at = models.DateTimeField(verbose_name=_('created_at'), editable=False, default=timezone.now)

    class Meta:
        verbose_name = _('student')
        verbose_name_plural = _('students')

    def __str__(self):
        return '%s %s %s' % (self.first_name, self.name, self.last_name)

    def get_absolute_url(self):
        return "/students/%i/" % self.id


class StudentAddress(models.Model):
    student = models.ForeignKey(Student, verbose_name=_('student'), on_delete=models.DO_NOTHING, default=0)
    country = models.ForeignKey(Country, on_delete=models.DO_NOTHING, verbose_name=_('country'), default=0)
    region = models.ForeignKey(Region, on_delete=models.DO_NOTHING, verbose_name=_('region'), default=0)
    district = models.ForeignKey(District, on_delete=models.DO_NOTHING, verbose_name=_('district'), default=0)
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING, verbose_name=_('city'), default=0)
    address = models.CharField(verbose_name=_('address'), max_length=300)
    permanent = models.BooleanField(verbose_name=_('permanent'), default=True)
    current = models.BooleanField(verbose_name=_('current'), default=True)

    class Meta:
        verbose_name = _('student_address')
        verbose_name_plural = _('students_addresses')

    def __str__(self):
        return '%s' % self.student


class StudentPassport(models.Model):
    student = models.ForeignKey(Student, verbose_name=_('student'), on_delete=models.DO_NOTHING, default=0)
    series = models.CharField(max_length=5, verbose_name=_('series'), default=0)
    number = models.PositiveIntegerField(verbose_name=_('number'), default=0)
    issue = models.DateField(verbose_name=_('valid to'), null=False, blank=False)
    organization = models.CharField(verbose_name=_('issued'), max_length=30, default=0)
    idn = models.CharField(verbose_name=_('INN'), max_length=14, default=0)

    class Meta:
        verbose_name = _('student_passport')
        verbose_name_plural = _('students_passports')

    def __str__(self):
        return '%s' % self.idn


class StudentBirthPlace(models.Model):
    student = models.OneToOneField(Student, verbose_name=_('student'), on_delete=models.DO_NOTHING, default=0)
    country = models.ForeignKey(Country, on_delete=models.DO_NOTHING, verbose_name=_('country'), default=0)
    region = models.ForeignKey(Region, on_delete=models.DO_NOTHING, verbose_name=_('region'), default=0)
    district = models.ForeignKey(District, on_delete=models.DO_NOTHING, verbose_name=_('district'), default=0)
    city = models.ForeignKey(City, on_delete=models.DO_NOTHING, verbose_name=_('city'), default=0)

    class Meta:
        verbose_name = _('student_birthplace')
        verbose_name_plural = _('students_birthplace')

    def __str__(self):
        return '%s' % self.country


class StudentAttestat(models.Model):
    student = models.ForeignKey(Student, verbose_name=_('student'), on_delete=models.DO_NOTHING, default=0)
    series = models.CharField(verbose_name=_('series'), max_length=4, default=0)
    number = models.PositiveIntegerField(verbose_name=_('number'), default=0)
    date = models.DateField(verbose_name=_('date'), null=False, blank=False, default=timezone.now)
    country = models.ForeignKey(Country, verbose_name=_('country'), on_delete=models.DO_NOTHING, default=0)
    region = models.ForeignKey(Region, verbose_name=_('region'), on_delete=models.DO_NOTHING, default=0)
    district = models.ForeignKey(District, verbose_name=_('district'), on_delete=models.DO_NOTHING, default=0)
    city = models.ForeignKey(City, verbose_name=_('city'), on_delete=models.DO_NOTHING, default=None)
    school = models.CharField(verbose_name=_('school'), max_length=255, default="№")

    class Meta:
        verbose_name = _('certificate from school')
        verbose_name_plural = _('certificates from school')

    def __str__(self):
        return '%s' % self.number


class StudentDiploma(models.Model):
    student = models.ForeignKey(Student, verbose_name=_('student'), on_delete=models.DO_NOTHING, default=0)
    series = models.CharField(verbose_name=_('series'), max_length=10, default=0)
    number = models.CharField(verbose_name=_('number'), max_length=50, default=0)
    university = models.CharField(verbose_name=_('university'), max_length=150, default=0)
    date = models.DateField(verbose_name=_('date'), null=False, default="2000-01-01")

    class Meta:
        verbose_name = _('available diploma')
        verbose_name_plural = _('available diplomas')

    def __str__(self):
        return '%s' % self.number


class Prikaz(models.Model):
    student = models.ForeignKey(Student, verbose_name=_('student'), on_delete=models.DO_NOTHING, default=0)
    number = models.CharField(verbose_name=_('number'), max_length=15, default=0)
    name = models.CharField(verbose_name=_('name'), max_length=150, default=0)
    date = models.DateField(verbose_name=_('date'), null=False, default="2005-01-01")

    class Meta:
        verbose_name = _('decree')
        verbose_name_plural = _('decrees')

    def __str__(self):
        return '%s' % self.number


class DiplomaFromUniversity(models.Model):
    student = models.ForeignKey(Student, verbose_name=_('student'), on_delete=models.DO_NOTHING, default=0)
    series = models.CharField(verbose_name=_('series'), max_length=10, default=0)
    number = models.CharField(verbose_name=_('number'), max_length=50, default=0)
    university = models.CharField(verbose_name=_('university'), max_length=150, default=0)
    date = models.DateField(verbose_name=_('date'), null=False, default="2000-01-01")

    class Meta:
        verbose_name = _('issued diploma')
        verbose_name_plural = _('issued diplomas')

    def __str__(self):
        return '%s' % self.number


class StudentAdditionalInf(models.Model):
    student = models.ForeignKey(Student, verbose_name=_('student'), on_delete=models.DO_NOTHING, default=0)
    note = models.TextField(verbose_name=_('note'), default="Нет")

    class Meta:
        verbose_name = _('additional information')
        verbose_name_plural = _('additional informations')

    def __str__(self):
        return '%s' % self.note


# Educational department tables
class UchPlanGroup(models.Model):
    group = models.ForeignKey(Groups, verbose_name=_('group'), on_delete=models.DO_NOTHING, default=0)
    semestr = models.PositiveIntegerField(verbose_name=_('semestr'), default=0)
    subject = models.ForeignKey(Subject, verbose_name=_('subject'), on_delete=models.DO_NOTHING, default=0)
    teacher = models.ForeignKey(Staff, verbose_name=_('teacher'), on_delete=models.DO_NOTHING, default=0)
    clock = models.FloatField(verbose_name=_('clock'), default=0.0)
    credit = models.FloatField(verbose_name=_('credit'), default=0.0)
    control = models.IntegerField(verbose_name=_('control_b'), choices=CONTROL_TYPE, default=EXAM, blank=False)
    edited = models.DateTimeField(verbose_name=_('edited'), default=timezone.now, editable=False)

    class Meta:
        verbose_name = _('uch_pl_group')
        verbose_name_plural = _('uch_pl_groups')

    def __str__(self):
        return '%s %s %s %s' % (self.group, self.semestr, self.subject, self.get_control_display())


class UchPlanStudents(models.Model):
    group_code = models.ForeignKey(Groups, verbose_name=_('group_code'), on_delete=models.DO_NOTHING, default=0)
    student = models.ForeignKey(Student, verbose_name=_('student'), on_delete=models.DO_NOTHING, default=0)
    semestr = models.PositiveIntegerField(verbose_name=_('semestr'), null=False, default=1)
    disc_code = models.ForeignKey(Subject, verbose_name=_('disc_code'), on_delete=models.DO_NOTHING, default=0)
    clock = models.FloatField(verbose_name=_('clock'), default=0)
    credit = models.FloatField(verbose_name=_('credit'), default=0)
    first_module = models.PositiveIntegerField(verbose_name=_('first_module'), default=0)
    second_module = models.PositiveIntegerField(verbose_name=_('second_module'), default=0)
    writing = models.PositiveIntegerField(verbose_name=_('writing'), default=0)
    ball = models.PositiveIntegerField(verbose_name=_('ball'), default=0)
    ocenka = models.IntegerField(verbose_name=_('mark'), choices=MARKS_CHOISES, default=NOT_DEFINED, blank=True)
    controlb = models.IntegerField(verbose_name=_('control_b'), choices=CONTROL_TYPE, default=EXAM)
    begunok = models.BooleanField(verbose_name=_('begunok'), default=False)
    number_of_begunok = models.CharField(verbose_name=_('number_of_begunok'), max_length=30, default=0)
    number_of_vedomost = models.CharField(verbose_name=_('number_of_vedomost'), max_length=30, default=0)
    date = models.DateField(verbose_name=_('date'), null=True, blank=True)
    edited = models.DateTimeField(verbose_name=_('edited'), default=timezone.now, editable=False)
    date_oper = models.DateTimeField(verbose_name=_('date_oper'), default=timezone.now, editable=False)
    uchplsadd_date = models.DateTimeField(verbose_name=_('uchpls_add_date'), default=timezone.now, editable=False)
    uchplgroup_code = models.ForeignKey(UchPlanGroup,
                                        verbose_name=_('uchplgroup_code'),
                                        on_delete=models.DO_NOTHING,
                                        null=True, editable=False)

    objects = models.Manager()
    uchpl_m = UchPlManager()

    class Meta:
        verbose_name = _('uch_plan_student')
        verbose_name_plural = _('uch_plan_students')

    def __str__(self):
        return '%s %s %s %s %s %s ' % (self.student, self.group_code,
                                       self.disc_code, self.semestr,
                                       self.ocenka, self.controlb)

    def save(self, *args, **kwargs):
        if self.begunok:
            self.ball = self.first_module + self.second_module + self.writing
            self.ocenka = marks_class.return_grade(self.controlb, self.disc_code.id, self.ball)
            self.date = timezone.now()
            self.full_clean()
            super(UchPlanStudents, self).save(*args, **kwargs)
        else:
            if marks_settings.is_available and marks_settings.is_date_available:
                self.ball = self.first_module + self.second_module + self.writing
                self.ocenka = marks_class.return_grade(self.controlb, self.disc_code.id, self.ball)
                self.edited = timezone.now()
                self.full_clean()
                super(UchPlanStudents, self).save(*args, **kwargs)


class UchplanStudentsVodomosti(UchPlanStudents):
    class Meta:
        proxy = True
        verbose_name = _('vedomost')
        verbose_name_plural = _('vedomosti')
