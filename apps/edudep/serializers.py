from rest_framework import serializers
from .models import Student, UchPlanStudents, UchPlanGroup
from apps.core.models import Groups


class StudentSerializer(serializers.ModelSerializer):
    group_code = serializers.StringRelatedField()

    class Meta:
        model = Student
        fields = ('id', 'group_code', 'first_name', 'name', 'last_name')


class UchPlanStudentsListSerializer(serializers.ModelSerializer):

    class Meta:
        model = UchPlanStudents
        fields = '__all__'


class UchPlanStudentsDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = UchPlanStudents
        fields = '__all__'

    def change_marks(self, validated_data):
        uchpl_stud = UchPlanStudents.objects.filter(pk=validated_data['id'])\
            .update(
            first_module=validated_data['first_module'],
            second_module=validated_data['second_module'],
            writing=validated_data['writing']
        )
        uchpl_stud.save()
        return uchpl_stud



#         (first_module, second_module, writing, ball, ocenka, controlb, begunok, number_of_begunok: CharField
# number_of_vedomost: CharField
# date: DateField)


# class UchplansOfGroupSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = UchPlanGroup
#         fields = "__all__"


class UchPlanGroupSerializer(serializers.ModelSerializer):
    # uc_plans = serializers.SerializerMethodField('get_uchplans')
    #
    # def get_uchplans(self, group=None):
    #     plans = UchPlanGroup.objects.filter(group=group)
    #     serializer_plans = UchplansOfGroupSerializer(instance=plans, many=True)
    #
    #     mapped_object = {
    #         'uch_plans': serializer_plans.data,
    #     }
    #
    #     return mapped_object
    #
    # class Meta:
    #     model = Groups
    #     fields = ('id', 'code', 'uc_plans')
    class Meta:
        model = UchPlanGroup
        fields = "__all__"
