from django.views.generic import DetailView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Count
import datetime
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from apps.edudep.models import Student, Groups, Prikaz, StudentAddress
from apps.edudep.models import UchPlanGroup, UchPlanStudents, StudentBirthPlace
from apps.edudep.models import StudentAttestat, StudentDiploma
from apps.core.models import Settings, MarksSettings
from apps.reports.utils import WorkWithExcel
from apps.reports.serializers import StudReportByDepSerializer, StudReportByNatSerializer


# Create your views here.
class StudentSpravkaView(LoginRequiredMixin, DetailView):
    login_url = 'admin/login'
    template_name = 'reports/student_spravka.html'
    queryset = Student.objects.select_related()
    context_object_name = 'student'

    def get_context_data(self, **kwargs):
        context = super(StudentSpravkaView, self).get_context_data(**kwargs)        
        context["settings_kg"] = Settings.objects.filter(language=0).first()
        context["settings_ru"] = Settings.objects.filter(language=1).first()
        context["settings_en"] = Settings.objects.filter(language=2).first()
        context["group"] = Groups.objects.get(code=self.object.group_code)
        return context


class StudentCardView(LoginRequiredMixin, DetailView):
    login_url = 'admin/login'
    template_name = 'reports/student_card.html'
    queryset = Student.objects.select_related()
    context_object_name = 'student'

    def get_context_data(self, **kwargs):
        context = super(StudentCardView, self).get_context_data(**kwargs)
        context["settings"] = Settings.objects.filter(active=True).first()
        context["group"] = Groups.objects.filter(code=self.object.group_code)
        context["prikaz"] = Prikaz.objects.filter(student=self.object.id).order_by('id')
        context["address"] = StudentAddress.objects.filter(student=self.object.id,
                                                           permanent=True, current=True)
        context["birthplace"] = StudentBirthPlace.objects.filter(student=self.object.id)
        context["attestat"] = StudentAttestat.objects.filter(student=self.object.id)
        context["diploma"] = StudentDiploma.objects.filter(student=self.object.id)
        return context


class StudentsMovementsJournalView(LoginRequiredMixin, DetailView):
    login_url = 'admin/login'
    template_name = 'reports/stud_movements_journal.html'
    queryset = Groups.objects.select_related()
    context_object_name = 'group'

    def get_context_data(self, **kwargs):
        context = super(StudentsMovementsJournalView, self).get_context_data(**kwargs)
        context["settings_ru"] = Settings.objects.select_related().get(active=True)
        try:
            context["student_list"] = Student.objects.filter(group_code=self.object).order_by('first_name',
                                                                                              'name', 'last_name')
        except Student.DoesNotExist:
            context["student_list"] = []
        try:
            context["prikaz"] = Prikaz.objects.select_related()
        except Student.DoesNotExist:
            context["prikaz"] = []
        return context


class StudentsListInGroupView(LoginRequiredMixin, DetailView):
    login_url = 'admin/login'
    template_name = 'reports/students_list_in_group.html'
    queryset = Groups.objects.select_related()
    context_object_name = 'group'

    def get_context_data(self, **kwargs):
        context = super(StudentsListInGroupView, self).get_context_data(**kwargs)
        try:
            context["student_list"] = Student.objects.filter(group_code=self.object)
        except Student.DoesNotExist:
            context["student_list"] = []
        try:
            context["settings"] = Settings.objects.filter(active=True).first()
        except Settings.DoesNotExist:
            context["settings"] = []
        return context


class UchPlanGroupCurrentDetailPDFView(LoginRequiredMixin, DetailView):
    login_url = '/admin/login/'
    template_name = 'edudep/current_vedom.html'
    queryset = UchPlanGroup.objects.select_related()
    context_object_name = 'uchplgr'

    def get_context_data(self, **kwargs):
        context = super(UchPlanGroupCurrentDetailPDFView, self).get_context_data(**kwargs)
        context["settings_ru"] = Settings.objects.select_related().get(active=True)
        context["groups"] = Groups.objects.get(code=self.object.group)
        context["marks"] = MarksSettings.objects.filter().first()
        context["students_m"] = UchPlanStudents.objects.filter(semestr=self.object.semestr,
                                                               disc_code_id=self.object.subject_id,
                                                               group_code_id=self.object.group
                                                               ).order_by('student__first_name')
        return context


class UchPlanGroupDetailPDFView(LoginRequiredMixin, DetailView):
    login_url = '/admin/login/'
    template_name = 'edudep/vedom.html'
    queryset = UchPlanGroup.objects.select_related()
    context_object_name = 'uchplgr'

    def get_context_data(self, **kwargs):
        context = super(UchPlanGroupDetailPDFView, self).get_context_data(**kwargs)
        context["settings_ru"] = Settings.objects.select_related().get(active=True)
        context["groups"] = Groups.objects.get(code=self.object.group)
        context["marks"] = MarksSettings.objects.select_related().get(pk=1)
        context["students_m"] = UchPlanStudents.objects.filter(semestr=self.object.semestr,
                                                               disc_code_id=self.object.subject_id,
                                                               group_code_id=self.object.group
                                                               ).order_by('student__first_name')
        return context


class UchPlanStudentsBegunok(LoginRequiredMixin, DetailView):
    login_url = '/admin/login/'
    template_name = 'edudep/begunok.html'
    queryset = UchPlanStudents.objects.select_related()
    context_object_name = 'begunok'

    def get_context_data(self, **kwargs):
        context = super(UchPlanStudentsBegunok, self).get_context_data(**kwargs)
        context["settings_ru"] = Settings.objects.select_related().get(active=True)
        context["groups"] = Groups.objects.get(code=self.object.group_code)
        context["until_date"] = datetime.datetime.today() + datetime.timedelta(10)
        return context


class UchPlanStudentsCurrSemestr(LoginRequiredMixin, DetailView):
    login_url = '/admin/login/'
    template_name = 'reports/curr_uchplan.html'
    queryset = Student.objects.select_related()
    context_object_name = 'student'

    def get_context_data(self, **kwargs):
        context = super(UchPlanStudentsCurrSemestr, self).get_context_data(**kwargs)
        context["settings_ru"] = Settings.objects.select_related().get(active=True)
        context["groups"] = Groups.objects.get(code=self.object.group_code)
        context["uchplans"] = UchPlanStudents.objects.filter(student=self.object.id, semestr=self.object.semestr)


class UchPlanStudentsSemestr(LoginRequiredMixin, DetailView):
    login_url = '/admin/login/'
    template_name = 'reports/curr_uchplan.html'
    queryset = Student.objects.select_related()
    context_object_name = 'student'

    def get_context_data(self, **kwargs):
        context = super(UchPlanStudentsSemestr, self).get_context_data(**kwargs)
        context["settings_ru"] = Settings.objects.select_related().get(active=True)
        context["groups"] = Groups.objects.get(code=self.object.group_code)
        context["uchplans"] = UchPlanStudents.objects.filter(student=self.object.id, semestr=self.kwargs['sem'])
        return context


class JournalOfMarks(LoginRequiredMixin, DetailView):
    login_url = '/admin/login/'
    template_name = 'reports/curr_uchplan.html'
    queryset = Groups.objects.select_related()
    context_object_name = 'groups'
    WorkWithExcel.create_marks_journal()

    def get_context_data(self, **kwargs):
        context = super(JournalOfMarks, self).get_context_data(**kwargs)
        context["students"] = Student.objects.filter(group_code=self.object.group_code)
        context["uchplan"] = UchPlanGroup.objects.filter(group=self.object.group_code,
                                                         semestr=self.object.semestr)
        context["uchplan_stud"] = UchPlanStudents.objects.filter(group_code=self.object.group_code,
                                                                 semestr=self.object.semestr)
        return context


class StudentByDepartments(LoginRequiredMixin, ListView):
    login_url = 'admin/login'
    template_name = 'reports/students_by_department.html'
    queryset = Student.objects.select_related()
    context_object_name = 'students'

    def get_context_data(self, **kwargs):
        context = super(StudentByDepartments, self).get_context_data(**kwargs)
        studs = Student.objects.filter(learning_form_id__id__range=(1, 4)).select_related()\
            .values('learning_form_id__name').annotate(dcound=Count('learning_form_id__name'))
        context["by_deps"] = studs
        context["by_deps_count"] = sum(i['dcound'] for i in studs)
        return context


class StudentByNationality(LoginRequiredMixin, ListView):
    login_url = 'admin/login'
    template_name = 'reports/students_by_nationality.html'
    queryset = Student.objects.select_related()
    context_object_name = 'students'

    def get_context_data(self, **kwargs):
        context = super(StudentByNationality, self).get_context_data(**kwargs)
        studs = Student.objects.filter(learning_status_id__name = "Обучение").select_related()\
            .values('nationality_id__name').annotate(dcound=Count('nationality_id__name')).order_by('-dcound')
        context["by_nations"] = studs
        context["by_nations_count"] = sum(i['dcound'] for i in studs)
        return context