from rest_framework import serializers
from apps.edudep.models import Student


class StudReportByDepSerializer(serializers.Serializer):

    class Meta:
        model = Student


class StudReportByNatSerializer(serializers.Serializer):

    class Meta:
        model = Student
