from django.contrib import admin
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from apps.edudep.admin import StudentAdmin
from apps.edudep.models import Student
from apps.core.admin import GroupsAdmin
from apps.core.models import Groups


# Register your models here.
class GroupStudentsReport(Student):
    class Meta:
        verbose_name = _('student')
        verbose_name_plural = _('students')
        proxy = True
        app_label = 'reports'


class GroupsReport(Groups):
    class Meta:
        verbose_name = _('group')
        verbose_name_plural = _('groups')
        proxy = True
        app_label = 'reports'


@admin.register(GroupStudentsReport)
class GroupStudentsReportAdmin(StudentAdmin):
    list_display = ('first_name', 'name', 'last_name', 'group_code', 'open_student_print_in_frontpage',)
    list_filter = (
        'group_code',
    )

    def open_student_print_in_frontpage(self, obj):
        return format_html("<a type='submit' class='button' href='/reports/student/{}/spravka', target='_blank'>"
                           "Справка</a> "
                           "<a type='submit' class='button' href='/reports/student/{}/studentcard', target='_blank'>"
                           "Уч. карточка</a> ",
                           obj.pk,
                           obj.pk,)

    open_student_print_in_frontpage.short_description = _('print')

    def has_add_permission(self, request):
        return False


@admin.register(GroupsReport)
class GroupsReport(GroupsAdmin):
    list_display = ('code', 'name', 'semestr', 'learningform', 'department', 'open_group_print_in_frontpage')
    list_filter = (
        'department', 'learningform',
    )

    def open_group_print_in_frontpage(self, obj):
        return format_html("<a type='submit' class='button' href='/reports/group/{}/journal_students', target='_blank'>"
                           "Журнал учета дв. студ</a> "
                           "<a type='submit' class='button' href='/reports/group/{}/studentsingr', target='_blank'>"
                           "Список студ.</a>",
                           obj.pk,
                           obj.pk,)

    open_group_print_in_frontpage.short_description = _('print')

    def has_add_permission(self, request):
        return False
