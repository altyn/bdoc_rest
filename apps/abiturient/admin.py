from django.contrib import admin
from django.utils.html import format_html
from .models import Abiturient, AbitOrt, AbitSubject, Exams
from django.utils.translation import ugettext_lazy as _


# Register your models here.
class AbitOrtInline(admin.StackedInline):
    model = AbitOrt
    extra = 0
    show_change_link = True
    classes = ['collapse']


class ExamsInline(admin.StackedInline):
    model = Exams
    extra = 0
    show_change_link = True
    classes = ['collapse']


@admin.register(Abiturient)
class AbiturientAdmin(admin.ModelAdmin):
    inlines = [AbitOrtInline, ]
    list_display = ('regnumber', 'lastname', 'name', 'middlename', 'point', 'tour', 'department', 'learningform',)
    list_filter = (
        'department',
        'learningform', 
        'tour',
        'year',
    )
    
    def get_queryset(self, request):
        return self.model.objects.filter(exam=False)
    

@admin.register(AbitSubject)
class AbitSubjectAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


class ExamAbiturient(Abiturient):
    class Meta:
        verbose_name = _('inner_abiturient')
        verbose_name_plural = _('inner_abiturients')
        proxy=True
    

@admin.register(ExamAbiturient)
class ExamAbiturientAdmin(AbiturientAdmin):
    inlines = [ExamsInline, ]

    def get_queryset(self, request):
        return self.model.objects.filter(exam=True)

