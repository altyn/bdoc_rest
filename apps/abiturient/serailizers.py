from rest_framework import serializers
from .models import Abiturient


class AbiturientSerializer(serializers.ModelSerializer):
    department = serializers.StringRelatedField()
    learningform = serializers.StringRelatedField()

    class Meta:
        model = Abiturient
        fields = (
            "id", "regnumber", "lastname", "name", "middlename",
            "point", "tour", "exam",
            "year", "department", "learningform"
        )