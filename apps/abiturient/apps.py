from django.apps import AppConfig


class AbiturientConfig(AppConfig):
    name = 'apps.abiturient'
    verbose_name = 'Приемная комиссия'
