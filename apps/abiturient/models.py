from django.db import models
from apps.core.models import Department
from apps.catalogue.models import LearningForm, ORT_CERT_COLOR, DEFAULT_COLOR
from django.utils.translation import ugettext_lazy as _


EXAM = 0
TEST_WR = 1
TEST_ONLINE = 2
INTERVIEW = 3
EXAM_TYPE = (
    (EXAM, 'Экзамен письменный'),
    (TEST_WR, 'Тест письменный'),
    (TEST_ONLINE, 'Тест онлайн'),
    (INTERVIEW, 'Собеседование'),
)


# Create your models here.
class Abiturient(models.Model):
    id = models.AutoField(db_column="id", verbose_name=_('id'), primary_key=True)
    regnumber = models.CharField(db_column="regnumber", verbose_name=_('regnumer'), max_length=255, null=False, blank=False)
    lastname = models.CharField(db_column="lastname", verbose_name=_('first_name'), max_length=255, null=False, blank=False)
    name = models.CharField(db_column="name", verbose_name=_('name'), max_length=255, null=False, blank=True)
    middlename = models.CharField(db_column="middlename", verbose_name=_('last_name'), max_length=255, null=True, blank=True)
    point = models.PositiveIntegerField(db_column="point", verbose_name=_('ball'), default=0)
    tour = models.PositiveIntegerField(db_column="tour", verbose_name=_('tour'), default=1)
    department = models.ForeignKey(Department, db_column="department", verbose_name=_('department'), on_delete=models.SET_NULL, null=True)
    learningform = models.ForeignKey(LearningForm, db_column="leaningform", verbose_name=_('learning_form'), on_delete=models.SET_NULL, null=True)
    phone = models.IntegerField(db_column="phone", verbose_name=_('phone'), null=True, blank=True)
    exam = models.BooleanField(db_column="exam", verbose_name=_('exam'), default=False)
    year = models.DateField(db_column="year", verbose_name=_('year'), null=False)

    class Meta:
        db_table = "abiturient"
        verbose_name = _('abiturient')
        verbose_name_plural = _('abiturients')
        get_latest_by = "id"
        ordering=["-regnumber", "-year"]

    def __str__(self):
        return "%s" % self.regnumber


class AbitOrt(models.Model):
    id = models.AutoField(db_column="id", verbose_name=_('id'), primary_key=True)
    abiturient = models.OneToOneField(Abiturient, db_column="abiturient", verbose_name=_('abiturient'), on_delete=models.SET_NULL, null=True)
    code = models.IntegerField(db_column="code", verbose_name=_('cert_code'), default=0, null=False, blank=False)
    color = models.CharField(db_column="color", verbose_name=_('colour'), max_length=10, choices=ORT_CERT_COLOR, default=DEFAULT_COLOR)
    main_test = models.PositiveIntegerField(db_column="main_test", verbose_name=_('main_test'), default=0, null=False, blank=False)
    english = models.PositiveIntegerField(db_column="english", verbose_name=_('english_language'), default=0)
    math = models.PositiveIntegerField(db_column="math", verbose_name=_('math'), default=0)
    history = models.PositiveIntegerField(db_column="history", verbose_name=_('history'), default=0)
    physics = models.PositiveIntegerField(db_column="physics", verbose_name=_('physics'), default=0)
    biology = models.PositiveIntegerField(db_column="biology", verbose_name=_('biology'), default=0)
    chemistry = models.PositiveIntegerField(db_column="chemistry", verbose_name=_('chemistry'), default=0)
    year = models.DateField(db_column="year", verbose_name=_('year'), auto_now_add=True)
    
    class Meta:
        db_table = "abiturort"
        verbose_name = _('abitur_cert')
        verbose_name_plural = _('abitur_certs')
        get_latest_by = "id"
        ordering = ["-id"]

    def get_summ_of_points(self, sum_of_point=0):
        point_agg = AbitOrt.objects.annotate(
            summ_point=models.F('main_test') 
                + models.F('english')+ models.F('math')
                + models.F('history')+ models.F('physics')
                + models.F('biology')+ models.F('chemistry'))
        return sum_of_point

    def __str__(self):
        return "%s" % self.abiturient


class AbitSubject(models.Model):
    id = models.AutoField(db_column="id", verbose_name=_('id'), primary_key=True)
    name = models.CharField(db_column="name", verbose_name=_('name_of_smth'), max_length=255)

    class Meta:
        db_table = "abit_subject"
        verbose_name = _('enrollment_exam')
        verbose_name_plural = _('enrollment_exams')
        ordering = ["-name"]

    def __str__(self):
        return "%s" % self.name


class Exams(models.Model):
    id = models.AutoField(db_column="id", verbose_name=_('id'), primary_key=True)
    abiturient = models.ForeignKey(Abiturient, db_column="abiturient_id", verbose_name=_('abiturient'),
                                   on_delete=models.SET_NULL, null=True, blank=True)
    subject = models.ForeignKey(AbitSubject, db_column="subject_id", verbose_name=_('subject'),
                                on_delete=models.SET_NULL, null=True, blank=True)
    exam_type = models.IntegerField(db_column="exam_type", verbose_name=_('type_of_control'),
                                    choices=EXAM_TYPE, default=INTERVIEW)
    date = models.DateField(db_column="date", verbose_name=_('date'), auto_now_add=True)
    point = models.FloatField(db_column="point", verbose_name=_('ball'), default=0)
    teacher = models.CharField(db_column="teacher", verbose_name=_('teacher'), max_length=300, blank=True)

    class Meta:
        db_table = "abiturexam"
        verbose_name = _('exam_of_abitur')
        verbose_name_plural = _('abitur_exams')
        ordering = ['-date']

    def __str__(self):
        return "%s" % self.subject

