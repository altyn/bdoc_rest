from rest_framework import generics
from django.views.generic import ListView, DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
import time
from .models import Abiturient, Exams
from .serailizers import AbiturientSerializer


# Create your views here.
class AbiturientsIndexList(LoginRequiredMixin, ListView):
    template_name = "abiturient/abiturient_list.html"
    model = Abiturient
    paginate_by = 20
    queryset = Abiturient.objects.filter(exam=False, year__year=time.strftime("%Y"))
    context_object_name = "abiturient_list"


class AbiturientsIndex(LoginRequiredMixin, ListView):
    template_name = "abiturient/abiturient_inner_list.html"
    model = Abiturient
    paginate_by = 20
    queryset = Abiturient.objects.filter(exam=True, year__year=time.strftime("%Y"))
    context_object_name = "abiturient_list"


class AbiturientView(LoginRequiredMixin, DetailView):
    login_url = '/admin/login/'
    template_name = 'abiturient/abiturient_inner.html'
    queryset = Abiturient.objects.filter(exam=True).select_related()
    context_object_name = 'abiturient'

    def get_context_data(self, **kwargs):
        context = super(AbiturientView, self).get_context_data(**kwargs)
        try:
            context["exam_list"] = Exams.objects.select_related().filter(abiturient=self.object)
        except Exams.DoesNotExist:
            context["exam_list"] = []
        return context


class AbiturientAPIView(generics.ListCreateAPIView):
    """
    List all abiturients, or create a new snippet.
    """
    queryset = Abiturient.objects.filter(exam=False, year__year=time.strftime("%Y"))
    serializer_class = AbiturientSerializer


class AbiturientDetailAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update or delete a abiturient instance.
    """
    queryset = Abiturient.objects.all()
    serializer_class = AbiturientSerializer
