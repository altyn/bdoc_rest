from django.apps import AppConfig


class IntdepConfig(AppConfig):
    name = 'apps.intdep'
    verbose_name = 'Международный отдел'

