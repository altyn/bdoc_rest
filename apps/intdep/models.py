from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from apps.edudep.models import Student
from apps.hr.models import Staff


# Create your models here.
WAITING = 0
PENDING = 1
FINISHED = 2
STATUS_CHOISES = (
    (WAITING, 'Waiting'),
    (PENDING, 'Pending'),
    (FINISHED, 'Finished'),
)


class InternshipsStudent(models.Model):
    student = models.ForeignKey(Student, verbose_name=_('student'), blank=False, null=False, default=0)
    where = models.CharField(max_length=500, verbose_name=_('where'))
    program = models.CharField(max_length=200, verbose_name=_('program'))
    date_from = models.DateField(default=timezone.now, verbose_name=_('date_from'))
    date_to = models.DateField(default=timezone.now, verbose_name=_('date_to'))
    status = models.IntegerField(choices=STATUS_CHOISES, default=WAITING, verbose_name=_('status'))

    class Meta:
        verbose_name = _('student_intership')
        verbose_name_plural = _('student_interships')

    def __str__(self):
        return '%s %s %s %s %s' % (self.student.last_name,
                                   self.student.name,
                                   self.where,
                                   self.program,
                                   self.date_from)


class InternshipsStaff(models.Model):
    staff = models.ForeignKey(Staff, verbose_name=_('staff'))
    where = models.CharField(max_length=500, verbose_name=_('where'))
    program = models.CharField(max_length=200, verbose_name=_('program'))
    date_from = models.DateField(verbose_name=_('date_from'))
    date_to = models.DateField(verbose_name=_('date_to'))

    class Meta:
        verbose_name = _('staff_intership')
        verbose_name_plural = _('staff_interships')

    def __str__(self):
        return '%s %s %s %s %s' % (self.staff.last_name,
                                   self.staff.name,
                                   self.where,
                                   self.program,
                                   self.date_from)
