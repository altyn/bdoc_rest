from django.contrib import admin
from .models import InternshipsStudent, InternshipsStaff


# Register your models here.
@admin.register(InternshipsStudent)
class IntershipStudentAdmin(admin.ModelAdmin):
    list_display = ('student', 'where', 'program', 'date_from', 'date_to', 'status')
    list_display_link = ('where', 'program')
    list_filter = (
        ('student', admin.RelatedOnlyFieldListFilter),
    )
admin.site.register(InternshipsStaff)
