from rest_framework import viewsets
from .serializers import StaffCvSerializer, StaffSerializer

from django.views.generic import DetailView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from apps.hr.models import Staff, StaffAddress
from apps.cv.models import AdditionalInf, Work, Conferences
from apps.cv.models import Education, Publications, Seminars
from apps.cv.models import LanguageKnowledge, Projects


class StaffCvViewSet(viewsets.ModelViewSet):
    queryset = Staff.objects.all()
    serializer_class = StaffCvSerializer


class StaffListViewSet(viewsets.ModelViewSet):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer


class CvStaffIndex(LoginRequiredMixin, ListView):
    template_name = "cv/cv_staff_list.html"
    model = Staff
    paginate_by = 12
    queryset = Staff.objects.all()
    context_object_name = "staff_list"


class GenerateCV(LoginRequiredMixin, DetailView):
    login_url = '/admin/login/'
    template_name = 'cv/cv_staff.html'
    queryset = Staff.objects.select_related()
    context_object_name = 'cv'

    def get_context_data(self, **kwargs):
        context = super(GenerateCV, self).get_context_data(**kwargs)
        try:
            context["address_list"] = StaffAddress.objects.select_related().get(staff=self.object, permanent=True)
        except StaffAddress.DoesNotExist:
            context["address_list"] = []
        context["conferences_list"] = Conferences.objects.filter(staff=self.object)
        context["education_list"] = Education.objects.filter(staff=self.object)
        context["work_list"] = Work.objects.filter(staff=self.object)
        context["addition_list"] = AdditionalInf.objects.filter(staff=self.object)
        context["publications_list"] = Publications.objects.filter(staff=self.object)
        context["seminars_list"] = Seminars.objects.filter(staff=self.object)
        context["langknowledge_list"] = LanguageKnowledge.objects.filter(staff=self.object)
        context["projects_list"] = Projects.objects.filter(staff=self.object)
        return context
