from django.apps import AppConfig


class CvConfig(AppConfig):
    name = 'apps.cv'
    verbose_name = 'Резюме ППС'
