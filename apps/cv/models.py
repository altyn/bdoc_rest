from django.db import models
from django.utils import timezone
from apps.hr.models import Staff
from apps.catalogue.models import Languages, LANGUAGE_LEVEL, ComputerExperience


# Create your models here.
class AdditionalInf(models.Model):
    staff = models.ForeignKey(Staff)
    communicate_exp = models.TextField(max_length=400)
    organization_exp = models.TextField(max_length=400)
    professional_exp = models.TextField(max_length=400)
    compexp_code = models.ForeignKey(ComputerExperience)
    compexp_desc = models.TextField(max_length=100)
    note = models.TextField(max_length=200)
    driver_license = models.CharField(max_length=10, default="Нет")

    class Meta:
        verbose_name = 'Дополнительная информация'
        verbose_name_plural = 'Дополнительная информация'

    def __str__(self):
        return '%s' % (self.staff)


class Conferences(models.Model):
    staff = models.ForeignKey(Staff)
    name = models.CharField(max_length=500)
    organization = models.CharField(max_length=500)
    location = models.CharField(max_length=200)
    date = models.DateField(default=timezone.now)

    class Meta:
        verbose_name = 'Конференция'
        verbose_name_plural = 'Конференции'

    def __str__(self):
        return '%s %s %s' % (self.staff,
                             self.name,
                             self.date)


class Education(models.Model):
    staff = models.ForeignKey(Staff)
    start = models.DateField(default=timezone.now)
    end = models.DateField(default=timezone.now)
    specialization = models.CharField(max_length=200)
    university = models.CharField(max_length=300)
    additional = models.CharField(max_length=300)

    class Meta:
        verbose_name = 'Образование'
        verbose_name_plural  = 'Образование'

    def __str__(self):
        return '%s %s %s' % (self.staff,
                             self.start,
                             self.end)


class LanguageKnowledge(models.Model):
    staff = models.ForeignKey(Staff)
    language = models.ForeignKey(Languages, related_name='language_code')
    understanding_listening = models.CharField(choices=LANGUAGE_LEVEL, max_length=3)
    understanding_reading = models.CharField(choices=LANGUAGE_LEVEL, max_length=3)
    speaking_interaction = models.CharField(choices=LANGUAGE_LEVEL, max_length=3)
    speaking_production = models.CharField(choices=LANGUAGE_LEVEL, max_length=3)
    writing = models.CharField(choices=LANGUAGE_LEVEL, max_length=3)

    class Meta:
        verbose_name = 'Знание языков'
        verbose_name_plural = 'Знание языков'

    def __str__(self):
        return '%s %s' % (self.staff, self.language)


class Projects(models.Model):
    staff = models.ForeignKey(Staff)
    name = models.CharField(max_length=500)
    date = models.DateField(default=timezone.now)
    duties = models.TextField(max_length=500)

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

    def __str__(self):
        return '%s %s' % (self.staff, self.name)


class Publications(models.Model):
    staff = models.ForeignKey(Staff)
    date = models.DateField(default=timezone.now)
    name = models.TextField(max_length=500)

    class Meta:
        verbose_name = 'Публикация'
        verbose_name_plural = 'Публикации'

    def __str__(self):
        return '%s %s' % (self.staff, self.name)


class Seminars(models.Model):
    staff = models.ForeignKey(Staff)
    name = models.TextField(max_length=500)
    date = models.DateField(default=timezone.now)

    class Meta:
        verbose_name = 'Семинар'
        verbose_name_plural = 'Семинары'

    def __str__(self):
        return '%s %s %s' % (self.staff, self.name, self.date)


class Work(models.Model):
    staff = models.ForeignKey(Staff)
    start = models.DateField(default=timezone.now)
    end = models.DateField(default=timezone.now)
    workplace = models.CharField(max_length=400)
    position = models.CharField(max_length=400)

    class Meta:
        verbose_name = 'Опыт работы'
        verbose_name_plural = 'Опыт работы'

    def __str__(self):
        return '%s %s %s' % (self.staff,
                             self.workplace,
                             self.position)
