from rest_framework import serializers
from apps.hr.models import Staff, StaffAddress
from apps.cv.models import AdditionalInf, Conferences, Education
from apps.cv.models import LanguageKnowledge, Publications, Seminars
from apps.cv.models import Work


class StaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = '__all__'


class StaffAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = StaffAddress
        fields = '__all__'


class AdditionalInfSerializer(serializers.ModelSerializer):
    class Meta:
        model = AdditionalInf
        fields = '__all__'


class EducationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Education
        fields = '__all__'


class WorkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Work
        fields = '__all__'


class ConferencesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Conferences
        fields = '__all__'


class LanguageKnowledgeSerializer(serializers.ModelSerializer):
    class Meta:
        model = LanguageKnowledge
        fields = '__all__'


class PublicationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Publications
        fields = '__all__'


class SeminarsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Seminars
        fields = '__all__'


class StaffCvSerializer(serializers.ModelSerializer):
    cvdata = serializers.SerializerMethodField('get_address')

    def get_address(self, staff=None):
        address = StaffAddress.objects.filter(staff=staff)
        serializer_adress = StaffAddressSerializer(instance=address, many=True)
        addition = AdditionalInf.objects.filter(staff=staff)
        serializer_addition = AdditionalInfSerializer(instance=addition, many=True)
        conferences = Conferences.objects.filter(staff=staff)
        serializer_conferences = ConferencesSerializer(instance=conferences, many=True)
        education = Education.objects.filter(staff=staff)
        serializer_education = EducationSerializer(instance=education, many=True)
        work = Work.objects.filter(staff=staff)
        serializer_work = WorkSerializer(instance=work, many=True)
        publications = Publications.objects.filter(staff=staff)
        serializer_publications = PublicationsSerializer(instance=publications, many=True)
        seminars = Seminars.objects.filter(staff=staff)
        serializer_seminars = SeminarsSerializer(instance=seminars, many=True)
        langknowledge = LanguageKnowledge.objects.filter(staff=staff)
        serializer_langknowledge = LanguageKnowledgeSerializer(instance=langknowledge, many=True)

        mapped_object = {
            'address': serializer_adress.data,
            'addition': serializer_addition.data,
            'education': serializer_education.data,
            'work': serializer_work.data,
            'conferences': serializer_conferences.data,
            'publications': serializer_publications.data,
            'seminars': serializer_seminars.data,
            'language': serializer_langknowledge.data
        }

        return mapped_object

    class Meta:
        model = Staff
        fields = ('id', 'last_name', 'cvdata')

