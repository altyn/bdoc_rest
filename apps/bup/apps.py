from django.apps import AppConfig


class BupConfig(AppConfig):
    name = 'apps.bup'
    verbose_name = 'Базовый учебный план'
