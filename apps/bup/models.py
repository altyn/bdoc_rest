from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from apps.core.models import Department
from apps.catalogue.models import LearningForm


# Тип работы в БУП
class BupJobType(models.Model):
    name = models.CharField(max_length=400, verbose_name=_('name_of_smth'), null=False, blank=False)

    class Meta:
        ordering = ['name']
        verbose_name = _('job_type_bup')
        verbose_name_plural = _('job_type_bup')

    def __str__(self):
        return '%s' % self.name


# Учебные годы
class BupYears(models.Model):
    start = models.DateField(verbose_name=_('start'), null=False, blank=False, default=timezone.now)
    end = models.DateField(verbose_name=_('end'), null=False, blank=False, default=timezone.now)

    class Meta:
        ordering = ['-start']
        verbose_name = _('academic_year')
        verbose_name_plural = _('academic_years')

    def __str__(self):
        return '%s'+'-'+'%s' % (self.start, self.end)


# Название планируемой работы
class BupJobCustomPlan(models.Model):
    name = models.CharField(verbose_name=_('name_of_smth'), max_length=500, null=False, blank=False, default='Не определено')
    plan = models.PositiveIntegerField(verbose_name=_('plan'), max_length=2, null=False, blank=False, default=0)
    job_type = models.ForeignKey(BupJobType, verbose_name=_('job_type'))

    class Meta:
        verbose_name = _('planning_job')
        verbose_name_plural = _('planning_jobs')

    def __str__(self):
        return '%s' % self.name


# Список Департаментов и форм обучения
class BupDepartmentList(models.Model):
    department = models.ForeignKey(Department, verbose_name=_('department'))
    learningForm = models.ForeignKey(LearningForm, verbose_name=_('learning_form'))

    class Meta:
        verbose_name = _('department_and_learning_form')
        verbose_name_plural = _('departments_and_learnings_form')

    def __str__(self):
        return '%s %s' % (self.department, self.learningForm)
