from django.apps import AppConfig


class PlatformConfig(AppConfig):
    name = 'apps'
    verbose_name = 'Платформа'
