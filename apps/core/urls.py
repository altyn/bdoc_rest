from django.conf.urls import url, include
from rest_framework import routers
from apps.edudep.views import (UchPlanStudentsListAPIView, UchPlanStudentDetailAPIView,
                               StudentsAPIView, StudentsDetailAPIView)
from apps.abiturient.views import AbiturientAPIView, AbiturientDetailAPIView

router = routers.DefaultRouter(trailing_slash=False)
router.include_format_suffixes = False

urlpatterns = [
    url(r'^', include(router.urls), name='api-root'),
    url(r'^uchplan/students/$', UchPlanStudentsListAPIView.as_view(), name='uchpl-list'),
    url(r'^uchplan/students/(?P<pk>\d+)/$', UchPlanStudentDetailAPIView.as_view(), name='uchpl-detail'),
    url(r'^abiturinets/$', AbiturientAPIView.as_view()),
    url(r'^abiturinets/(?P<pk>\d+)/$', AbiturientDetailAPIView.as_view()),
    url(r'^students/$', StudentsAPIView.as_view()),
    url(r'^students/(?P<pk>\d+)/$', StudentsDetailAPIView.as_view()),
]