from django.contrib import admin
from .models import University, Institution
from .models import Department, Groups
from .models import Settings, MarksSettings


@admin.register(Groups)
class GroupsAdmin(admin.ModelAdmin):
    list_display = ('id', 'code', 'name', 'semestr', 'curator', 'learningform', 'department', 'institution')
    list_display_link = ('id', 'code', 'name', 'semestr', 'curator', 'learningform', 'department')
    list_filter = (
        ('department', admin.RelatedOnlyFieldListFilter),
    )


@admin.register(Department)
class DepartmentsAdmin(admin.ModelAdmin):
    list_display = ('id', 'institution', 'name', 'header', 'assistant', 'phone', 'email')
    list_filter = (
         ('header', admin.RelatedFieldListFilter),
    )


@admin.register(University)
class UniversityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_editable = ('name',)


@admin.register(Institution)
class InstitutionAdmin(admin.ModelAdmin):
    list_display = ('id', 'university', 'shortname_kyr', 'header', 'phone', 'email',)
    list_display_links = ('id', 'shortname_kyr', 'phone', 'email',)


@admin.register(Settings)
class SettingsAdmin(admin.ModelAdmin):
    list_display = ('id', 'language', 'shortname', 'longname',)
    list_display_links = ('id', 'language', 'shortname', 'longname',)


@admin.register(MarksSettings)
class MarkSettingsAdmin(admin.ModelAdmin):
    list_display = ('id', 'year_start', 'year_end', 'allow_marks', 'allowmarks_date_start', 'allowmarks_date_end',)
    list_display_links = ('id', 'year_start',)

    # def has_add_permission(self, request):
    #     if MarksSettings.objects.all().count() > 0:
    #         return False
