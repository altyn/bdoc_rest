from django.db import models
from django.utils import timezone
from apps.catalogue.models import LearningForm
from django.utils.translation import ugettext_lazy as _
import datetime
import time


KYRGYZ = 0
RUSSIAN = 1
ENGLISH = 2
LANGUAGE_CHOISES = (
    (KYRGYZ, 'Кыргызча'),
    (RUSSIAN, 'Русский'),
    (ENGLISH, 'English'),
)


class University(models.Model):
    name = models.CharField(verbose_name=_('University'), max_length=150)

    class Meta:
        verbose_name = _('University')
        verbose_name_plural = _('Universities')

    def __str__(self):
        return self.name


class Institution(models.Model):
    name_kg = models.CharField(verbose_name=_('Institution(full name in KG)'), max_length=255, null=True)
    name_ru = models.CharField(verbose_name=_('Institution(full name in RU)'), max_length=255, null=True)
    name_en = models.CharField(verbose_name=_('Institution(full name in EN)'), max_length=255, null=True)
    shortname_kyr = models.CharField(verbose_name=_('Name(in short cyrillic)'), max_length=255, null=True)
    shortname_en = models.CharField(verbose_name=_('Name(in short latin)'), max_length=255, null=True)
    university = models.ForeignKey('University', related_name='university_of_inst',
                                   on_delete=models.SET_NULL, verbose_name=_('University'), null=True)
    header = models.ForeignKey('hr.Staff', related_name='institution_header',
                               on_delete=models.CASCADE, verbose_name=_('institution header'), null=True)
    phone = models.CharField(verbose_name=_('phone'), max_length=20, null=False)
    email = models.CharField(verbose_name=_('email'), max_length=50, null=False)

    class Meta:
        verbose_name = _('Institution')
        verbose_name_plural = _('Institutions') 

    def __str__(self):
        return self.name_ru


class Department(models.Model):
    id = models.IntegerField(primary_key=True, verbose_name=_('id'),auto_created=False)
    institution = models.ForeignKey('Institution', related_name=_('Institution'), on_delete=models.SET_NULL,
                                    verbose_name=_('Institution'), null=True)
    name = models.CharField(verbose_name=_('name'), max_length=50, null=False)
    header = models.ForeignKey('hr.Staff', related_name='department_header',
                               on_delete=models.CASCADE, verbose_name=_('header'), null=True)
    assistant = models.ForeignKey('hr.Staff', related_name='department_assistant',
                                  on_delete=models.CASCADE, verbose_name=_('assistant'), null=True)
    phone = models.CharField(verbose_name=_('phone'), max_length=20, null=False)
    email = models.CharField(verbose_name=_('email'), max_length=50, null=False)

    class Meta:
        verbose_name = _('department')
        verbose_name_plural = _('departments')

    def __str__(self):
        return self.name


class Groups(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=True)
    institution = models.ForeignKey('Institution', related_name='instituion_of_group', on_delete=models.SET_NULL,
                                    verbose_name=_('Institution'), null=True)
    department = models.ForeignKey(Department, verbose_name="Департамент", blank=False, null=False, default=0)
    code = models.CharField(verbose_name=_('group_code'), max_length=10, null=False)
    name = models.CharField(verbose_name=_('name_of_group'), max_length=50, null=False)
    semestr = models.IntegerField(verbose_name=_('semestr'), null=True, default=0)
    curator = models.ForeignKey('hr.Staff', verbose_name=_('curator'), blank=True, null=True)
    learningform = models.ForeignKey(LearningForm, verbose_name=_('Learning form'), blank=True, null=True)
    edited = models.DateTimeField(default=timezone.now, verbose_name=_('edited'), editable=False)
    graduated = models.BooleanField(verbose_name=_('Graduated'), default=False)

    class Meta:
        verbose_name = _('group')
        verbose_name_plural = _('groups')

    def __str__(self):
        return self.code


class Settings(models.Model):
    id = models.IntegerField(primary_key=True, verbose_name=_('id'), auto_created=True)
    language = models.IntegerField(choices=LANGUAGE_CHOISES, verbose_name=_('language'), default=KYRGYZ)
    shortname = models.CharField(verbose_name=_('shortname'), blank=True, max_length=255)
    longname = models.CharField(verbose_name=_('longname'), blank=True, max_length=500)
    address = models.CharField(verbose_name=_('address'), blank=True, max_length=500)
    phone = models.CharField(verbose_name=_('phone'), blank=True, max_length=15)
    fax = models.CharField(verbose_name=_('fax'), blank=True, max_length=15)
    okpo = models.CharField(verbose_name=_('okpo'), blank=True, max_length=25)
    inn = models.CharField(verbose_name=_('inn'), blank=True, max_length=14)
    rector = models.CharField(verbose_name=_('rector'), blank=True, max_length=120)
    rector_regalies = models.CharField(verbose_name=_('Rector regalies'), blank=True, max_length=100)
    glavbuh = models.CharField(verbose_name=_('Main accountant'), blank=True, max_length=120)
    uotdel = models.CharField(verbose_name=_('Educational Department'), blank=True, max_length=120)
    www = models.CharField(verbose_name=_('site'), blank=True, null=True, max_length=120)
    email = models.CharField(verbose_name=_('email'), blank=True, null=True, max_length=120)
    logo = models.CharField(verbose_name=_('logo'), blank=True, null=True, max_length=120, default='no_logo')
    active = models.BooleanField(db_column="active", verbose_name=_('Active settings'), default=False)

    class Meta:
        verbose_name = _('Main settings')
        verbose_name_plural = _('Main settings')

    def __str__(self):
        return '%s' % self.id


class MarksSettings(models.Model):
    year_start = models.DateField(verbose_name=_('year start'), blank=True)
    year_end = models.DateField(verbose_name=_('year_end'), blank=True)
    allow_marks = models.BooleanField(verbose_name=_('Allow marks'), default=False, null=False, blank=False)
    allowmarks_date_start = models.DateField(verbose_name=_('allow_date_start'), default=timezone.now, blank=False, null=False)
    allowmarks_date_end = models.DateField(verbose_name=_('allow_date_end'), default=timezone.now, blank=False, null=False)
    marks_high_end = models.IntegerField(verbose_name=_('marks_high_end'), blank=False, null=False, default=100)
    marks_high_start = models.IntegerField(verbose_name=_('marks_high_start'), blank=False, null=False, default=85)
    marks_well_end = models.IntegerField(verbose_name=_('marks_well_end'), blank=False, null=False, default=84)
    marks_well_start = models.IntegerField(verbose_name=_('marks_well_start'), blank=False, null=False, default=70)
    marks_satisfactorily_end = models.IntegerField(verbose_name=_('marks_satisfactorily_end'), blank=False, null=False, default=69)
    marks_satisfactorily_start = models.IntegerField(verbose_name=_('marks_satisfactorily_start'), blank=False, null=False, default=55)
    marks_bad_end = models.IntegerField(verbose_name=_('marks_bad_end'), blank=False, null=False, default=54)
    marks_bad_start = models.IntegerField(verbose_name=_('marks_bad_start'), blank=False, null=False, default=0)

    class Meta:
        verbose_name = _('Marks settings')
        verbose_name_plural = _('Marks settings')

    def __str__(self):
        return '%s' % self.id

    def is_available(self):
        return self.allow_marks

    def is_date_available(self):
        if self.allow_marks:
            start = time.mktime(datetime.datetime.strptime(str(self.allowmarks_date_start), "%Y-%m-%d").timetuple())
            today = time.mktime(datetime.datetime.now().timetuple())
            end = time.mktime(datetime.datetime.strptime(str(self.allowmarks_date_end), "%Y-%m-%d").timetuple())
            if start < today < end:
                return True
            else:
                return False
        else:
            return False
