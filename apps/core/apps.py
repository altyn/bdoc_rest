from django.apps import AppConfig


class NameConfig(AppConfig):
    name = 'apps.core'
    verbose_name = 'Основные'
