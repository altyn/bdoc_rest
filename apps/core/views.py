from rest_framework import viewsets
from .models import University, MarksSettings
from .serializers import UniversitySerializer, MarksSerializer

from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Department, Groups


class UniversityViewSet(viewsets.ModelViewSet):
    queryset = University.objects.all()
    serializer_class = UniversitySerializer


class DepartmentsIndex(LoginRequiredMixin, ListView):
    template_name = "core/departments.html"
    model = Department
    paginate_by = 12
    queryset = Department.objects.all()
    context_object_name = "department_list"


class GroupsIndex(LoginRequiredMixin, ListView):
    template_name = "core/groups.html"
    model = Department
    paginate_by = 12
    queryset = Groups.objects.all()
    context_object_name = "group_list"


class MarksViewSet(viewsets.ModelViewSet):
    queryset = MarksSettings.objects.all()
    serializer_class = MarksSerializer
