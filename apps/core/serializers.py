from rest_framework import serializers
from .models import University, MarksSettings


class UniversitySerializer(serializers.ModelSerializer):
    class Meta:
        model = University
        fields = '__all__'


class MarksSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarksSettings
        fields = ('year_start', 'year_end', 'allow_marks', 'allowmarks_date_start', 'allowmarks_date_end',
                  'marks_high_end', 'marks_high_start', 'marks_well_end', 'marks_well_start',
                  'marks_satisfactorily_end', 'marks_satisfactorily_start', 'marks_bad_end', 'marks_bad_start', )
        read_only_fields = ('year_start', 'year_end', 'allow_marks', 'allowmarks_date_start',
                            'allowmarks_date_end', 'marks_high_end', 'marks_high_start',
                            'marks_well_end', 'marks_well_start', 'marks_satisfactorily_end',
                            'marks_satisfactorily_start', 'marks_bad_end', 'marks_bad_start',)
