from django.apps import AppConfig


class SettingsConfig(AppConfig):
    name = 'app.settings'
    verbose_name = 'Основные настройки платформы'
