from django.apps import AppConfig


class AuthConfig(AppConfig):
    name = 'apps.auth'
    verbose_name = 'Настройки Авторизации'

    def ready(self):
        from . import signals
