from django.contrib.auth.models import User
from django.views.generic import DetailView
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, response, permissions
from .serializers import UserSerializer


# Create your views here.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = permissions.IsAuthenticated

    def retrieve(self, request, pk=None):
        if pk == 'i':
            return response.Response(UserSerializer(request.user, contex={'request': request}).data)
        return super(UserViewSet, self).retrieve(request, pk)


class UserProfileView(DetailView):
    model = User
    template_name = 'auth/user_profile.html'
    slug_field = 'id'
    slug_url_kwarg = 'id'
    context_object_name = 'user_p'

    # def get_object(self):
    #     return get_object_or_404(User, username=self.kwargs.get('username'))
