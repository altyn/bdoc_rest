from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from rest_framework.routers import DefaultRouter
from rest_framework.authtoken.views import obtain_auth_token

from .views import UserViewSet, UserProfileView

router = DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = router.urls

urlpatterns += [
    url(r'^token/$', csrf_exempt(obtain_auth_token)),
    url(r'^profile/(?P<pk>\d+)/$', UserProfileView.as_view(), name='user_profile'),
]