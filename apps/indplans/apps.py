from django.apps import AppConfig


class IndplansConfig(AppConfig):
    name = 'apps.indplans'
    verbose_name = 'Индивидуальные планы'
