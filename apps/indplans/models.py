from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from apps.core.models import Department, Groups
from apps.catalogue.models import LearningForm, Subject
from apps.hr.models import Staff


# Create your models here.
class BupYears(models.Model):
    start = models.DateField(verbose_name=_('start'), default=timezone.now, null=False, blank=False)
    end = models.DateField(verbose_name=_('end'), default=timezone.now, null=False, blank=False)

    class Meta:
        verbose_name = _('academic_year')
        verbose_name_plural = _('academic_years')

    def __str__(self):
        return '%s %s' % (self.start, self.end)


class BupJobType(models.Model):
    name = models.CharField(verbose_name=_('name_of_smth'), max_length=500, default=0)

    class Meta:
        verbose_name = _('job_type_bup')
        verbose_name_plural = _('job_type_bup')

    def __str__(self):
        return '%s' % self.name


class BupJobName(models.Model):
    name = models.CharField(verbose_name=_('name_of_smth'), max_length=400, default=0)
    plan = models.PositiveIntegerField(verbose_name=_('plan'), default=0)
    job_type = models.ForeignKey(BupJobType, verbose_name=_('job_type_bup'), null=True, blank=True)

    class Meta:
        verbose_name = _('job_name_bup')
        verbose_name_plural = _('jobs_name_bup')

    def __str__(self):
        return '%s %s' % (self.name, self.plan)


class BupDepList(models.Model):
    department = models.ForeignKey(Department, verbose_name=_('department'), null=True, blank=True)
    learningform = models.ForeignKey(LearningForm, verbose_name=_('learning_form'), null=True, blank=True)

    class Meta:
        verbose_name = _('bup_dep_list')
        verbose_name_plural = _('bup_dep_list')

    def __str__(self):
        return '%s %s' % (self.department, self.learningform)


class Bup(models.Model):
    disс_code = models.ForeignKey(Subject, verbose_name=_('subject'), null=True, blank=True)
    vsego_chas = models.PositiveIntegerField(default=0, verbose_name=_('vsego_chas'))
    kredit_vsego = models.FloatField(default=0, verbose_name=_('kredit_vsego'))
    aud_chas = models.FloatField(default=0, verbose_name=_('aud_chas'))
    lec_chas = models.FloatField(default=0, verbose_name=_('lec_chas'))
    prac_chas = models.FloatField(default=0, verbose_name=_('prac_chas'))
    ind_chas = models.FloatField(default=0, verbose_name=_('ind_chas'))
    srs_chas = models.FloatField(default=0, verbose_name=_('ind_chas'))
    zachet = models.FloatField(default=0, verbose_name=_('zachet'))
    ekzamen = models.FloatField(default=0, verbose_name=_('ekzamen'))
    kur_rabota = models.PositiveIntegerField(default=0, verbose_name=_('kurs_rabota'))
    kontr_rab = models.FloatField(default=0, verbose_name=_('kontr_rabota'))
    nir = models.PositiveIntegerField(default=0, verbose_name=_('nir'))
    semestr = models.PositiveIntegerField(default=1, verbose_name=_('semestr'))
    dep_list = models.ForeignKey(BupDepList, null=True, blank=True, verbose_name=_('bup_dep_list'))

    class Meta:
        verbose_name = _('bup')
        verbose_name_plural = _('bups')

    def __str__(self):
        return '%s %s %s' % (self.disс_code, self.semestr, self.dep_list_id)


class BupRaschetChasov(models.Model):
    disc_code = models.ForeignKey(Subject, null=True, blank=True, verbose_name=_('subject'))
    years = models.ForeignKey(BupYears, null=True, blank=True, verbose_name=_('academic_years'))
    gr_code = models.ForeignKey(Groups, null=True, blank=True, verbose_name=_('group'))
    student_kolvo = models.PositiveIntegerField(default=0, verbose_name=_('student_kolvo'))
    semestr = models.PositiveIntegerField(default=1, verbose_name=_('semestr'))
    group_course = models.PositiveIntegerField(default=1, verbose_name=_('group_course'))
    bup_lecture = models.PositiveIntegerField(default=0, verbose_name=_('bup_lecture'))
    lecture = models.PositiveIntegerField(default=0, verbose_name=_('lecture'))
    bup_seminar = models.PositiveIntegerField(default=0, verbose_name=_('bup_seminar'))
    seminar = models.PositiveIntegerField(default=0, verbose_name=_('seminar'))
    bup_kurs_rab = models.PositiveIntegerField(default=0, verbose_name=_('bup_kurs_rabota'))
    kurs_rab = models.PositiveIntegerField(default=0, verbose_name=_('kurs_rabota'))
    bup_kontr_rab = models.PositiveIntegerField(default=0, verbose_name=_('bup_kontr_rabota'))
    kontr_rab = models.PositiveIntegerField(default=0, verbose_name=_('kontr_rabota'))
    bup_nir = models.PositiveIntegerField(default=0, verbose_name=_('bup_nir'))
    nir = models.PositiveIntegerField(default=0, verbose_name=_('nir'))
    individual = models.PositiveIntegerField(default=0, verbose_name=_('individual'))
    konsult = models.PositiveIntegerField(default=0, verbose_name=_('konsult'))
    konsult_individ = models.PositiveIntegerField(default=0, verbose_name=_('konsult_individual'))
    srs = models.PositiveIntegerField(default=0, verbose_name=_('srs'))
    practica = models.PositiveIntegerField(default=0, verbose_name=_('practica'))
    zachet = models.PositiveIntegerField(default=0, verbose_name=_('zachet'))
    ekzamen = models.PositiveIntegerField(default=0, verbose_name=_('ekzamen'))
    preddipl_pract = models.PositiveIntegerField(default=0, verbose_name=_('preddipl_pract'))
    rukovodstvo_vkr = models.PositiveIntegerField(default=0, verbose_name=_('rukovodstvo_vkr'))
    recenziya = models.PositiveIntegerField(default=0, verbose_name=_('recenziya'))
    uchastie_v_gak = models.PositiveIntegerField(default=0, verbose_name=_('uchastie_v_gak'))
    vsego_chasov = models.PositiveIntegerField(default=0, verbose_name=_('vsego_chasov'))
    kredits = models.PositiveIntegerField(default=0, verbose_name=_('credits'))
    teacher = models.ForeignKey(Staff, null=True, blank=True, verbose_name=_('teacher'))
    dep_list = models.ForeignKey(BupDepList, null=True, blank=True, verbose_name=_('bup_dep_list'))

    class Meta:
        verbose_name = _('raschet_chasov')
        verbose_name_plural = _('raschet_chasov')

    def __str__(self):
        return '%s' % self.id


class IndPlanJobs(models.Model):
    teacher = models.ForeignKey(Staff, null=True, blank=True, verbose_name=_('teacher'))
    bup_job_type = models.ForeignKey(BupJobType, null=True, blank=True, verbose_name=_('job_type_bup'))
    semestr = models.PositiveIntegerField(default=1, verbose_name=_('semestr'))
    years = models.ForeignKey(BupYears, null=True, blank=True, verbose_name=_('academic_years'))
    job_name = models.ForeignKey(BupJobName, null=True, blank=True, verbose_name=_('job_name_bup'))
    sept_plan = models.PositiveIntegerField(default=0, verbose_name=_('sept_plan'))
    sep_fact = models.PositiveIntegerField(default=0, verbose_name=_('sept_fact'))
    oct_plan = models.PositiveIntegerField(default=0, verbose_name=_('oct_plan'))
    oct_fact = models.PositiveIntegerField(default=0, verbose_name=_('oct_fact'))
    nov_plan = models.PositiveIntegerField(default=0, verbose_name=_('nov_plan'))
    nov_fact = models.PositiveIntegerField(default=0, verbose_name=_('nov_fact'))
    dec_plan = models.PositiveIntegerField(default=0, verbose_name=_('dec_plan'))
    dec_fact = models.PositiveIntegerField(default=0, verbose_name=_('dec_fact'))
    jan_plan = models.PositiveIntegerField(default=0, verbose_name=_('jan_plan'))
    jan_fact = models.PositiveIntegerField(default=0, verbose_name=_('jan_fact'))
    feb_plan = models.PositiveIntegerField(default=0, verbose_name=_('feb_plan'))
    feb_fact = models.PositiveIntegerField(default=0, verbose_name=_('feb_fact'))
    march_plan = models.PositiveIntegerField(default=0, verbose_name=_('march_plan'))
    march_fact = models.PositiveIntegerField(default=0, verbose_name=_('march_fact'))
    april_plan = models.PositiveIntegerField(default=0, verbose_name=_('april_plan'))
    april_fac = models.PositiveIntegerField(default=0, verbose_name=_('april_fact'))
    may_plan = models.PositiveIntegerField(default=0, verbose_name=_('may_plan'))
    may_fact = models.PositiveIntegerField(default=0, verbose_name=_('may_fact'))
    june_plan = models.PositiveIntegerField(default=0, verbose_name=_('june_plan'))
    june_fact = models.PositiveIntegerField(default=0, verbose_name=_('june_fact'))
    total_plan = models.PositiveIntegerField(default=0, verbose_name=_('total_plan'))
    total_fact = models.PositiveIntegerField(default=0, verbose_name=_('total_fact'))

    class Meta:
        verbose_name = _('ind_plan')
        verbose_name_plural = _('ind_plans')

    def __str__(self):
        return '%s %s %s %s %s' % (self.teacher_id, self.bup_job_type_id,
                                   self.semestr, self.years_id,
                                   self.job_name)


# Рабочий учебный план
class WorkEduPlan(models.Model):
    pass
