from django.contrib import admin

from .models import BupYears, BupJobType, BupJobName
from .models import BupDepList, Bup, BupRaschetChasov, IndPlanJobs

from jet.admin import CompactInline


@admin.register(BupYears)
class BupYearsAdmin(admin.ModelAdmin):
    list_display = ('id', 'start', 'end')


class BupJobNameAdmin(CompactInline):
    model = BupJobName
    extra = 0

@admin.register(BupJobType)
class BupJobTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    inlines = [ BupJobNameAdmin ]
    


@admin.register(BupDepList)
class BupDepListAdmin(admin.ModelAdmin):
    list_display = ('id', 'department', 'learningform')


@admin.register(Bup)
class BupAdmin(admin.ModelAdmin):
    pass


@admin.register(BupRaschetChasov)
class BupRaschetChasovAdmin(admin.ModelAdmin):
    pass


@admin.register(IndPlanJobs)
class IndPLanJobsAdmin(admin.ModelAdmin):
    pass
