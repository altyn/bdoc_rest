from django.views.generic import DetailView, ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from apps.indplans.models import IndPlanJobs


# Create your views here.
class EducationalWork(LoginRequiredMixin, ListView):
    template_name = "indplans/education_work.html"
    queryset = IndPlanJobs.objects.all()
