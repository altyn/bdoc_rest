from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework_swagger.views import get_swagger_view

from apps.home.views import IndexView
from apps.cv.views import GenerateCV, CvStaffIndex
from apps.core.views import DepartmentsIndex, GroupsIndex
from apps.edudep.views import (StudentsIndex, StudentDetailViewSet,
                               UchPlanGroupIndex, UchPlanStudentIndex,
                               UchPlanGroupDetailView, StudentCreateViewSet)
from apps.indplans.views import EducationalWork
from apps.abiturient.views import AbiturientsIndex, AbiturientView, AbiturientsIndexList
from apps.reports.views import StudentSpravkaView, StudentsMovementsJournalView
from apps.reports.views import (StudentsListInGroupView, UchPlanGroupCurrentDetailPDFView,
                                UchPlanStudentsBegunok, UchPlanStudentsCurrSemestr,
                                UchPlanStudentsSemestr, UchPlanGroupDetailPDFView,
                                StudentCardView, JournalOfMarks, StudentByDepartments,
                                StudentByNationality)


admin.site.site_header = 'BDOC Web'
schema_view = get_swagger_view(title='BDOC API')

urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^docs/api/', schema_view),
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include('apps.core.urls', namespace='core')),
    url(r'^auth/', include('apps.auth.urls', namespace='auth')),
    url(r'^cv/staff/$', CvStaffIndex.as_view(), name='cv_list'),
    url(r'^cv/staff/(?P<pk>\d+)/$', GenerateCV.as_view(), name='cv_id'),
    url(r'^departments/$', DepartmentsIndex.as_view(), name='departments_list'),
    url(r'^groups/$', GroupsIndex.as_view(), name='groups_list'),
    url(r'^students/$', StudentsIndex.as_view(), name='students_list'),
    url(r'^uchplans/groups/$', UchPlanGroupIndex.as_view(), name='uchplgroup_list'),
    url(r'^uchplans/groups/(?P<pk>\d+)/$', UchPlanGroupDetailView.as_view(), name='uchplgroup_detail'),
    url(r'^uchplans/students/$', UchPlanStudentIndex.as_view(), name='uchplstudent_list'),
    url(r'^uchplans/students/(?P<pk>\d+)/b/print/$', UchPlanStudentsBegunok.as_view(), name='uchplstudent_begunok'),
    url(r'^indplan/eduwork/$', EducationalWork.as_view(), name='eduwork_list'),
    url(r'^indplan/eduwork/(?P<pk>\d+)/$', EducationalWork.as_view(), name='eduwork_id'),
    url(r'^abitur/inner$', AbiturientsIndex.as_view(), name='abitur_inner_list'),
    url(r'^abitur/$', AbiturientsIndexList.as_view(), name='abitur_list'),
    url(r'^abitur/inner/(?P<pk>\d+)/$', AbiturientView.as_view(), name='abitur_inner_id'),
    url(r'^reports/student/(?P<pk>\d+)/spravka/$', StudentSpravkaView.as_view(), name='student_spravka'),
    url(r'^reports/group/(?P<pk>\d+)/journal_students/$', StudentsMovementsJournalView.as_view(), name='students_movements'),
    url(r'^reports/group/(?P<pk>\d+)/studentsingr/$', StudentsListInGroupView.as_view(), name='students_movements'),
    url(r'^reports/group/(?P<pk>\d+)/current_v/print/$', UchPlanGroupCurrentDetailPDFView.as_view(), name='current_vedomost_print'),
    url(r'^reports/group/(?P<pk>\d+)/v/print/$', UchPlanGroupDetailPDFView.as_view(), name='vedomost_print'),
    url(r'^reports/group/(?P<pk>\d+)/journal/download/$', JournalOfMarks.as_view(), name='journal_download'),
    url(r'^reports/student/(?P<pk>\d+)/curr_uchplan/$', UchPlanStudentsCurrSemestr.as_view(), name='curruchplan_print'),
    url(r'^reports/student/(?P<pk>\d+)/curr_uchplan/(?P<sem>\d+)/$', UchPlanStudentsSemestr.as_view(), name='semuchplan_print'),
    url(r'^reports/student/(?P<pk>\d+)/studentcard/$', StudentCardView.as_view(), name='student_card_print'),
    url(r'^reports/bydepartment/$', StudentByDepartments.as_view(), name='student_by_department'),
    url(r'^reports/bynationality/$', StudentByNationality.as_view(), name='student_by_nationality'),
    url(r'^student/new/$', StudentCreateViewSet.as_view(), name='student_create'),
    url(r'^student/(?P<pk>\d+)/$', StudentDetailViewSet.as_view(), name='student_detail'),
    url(r'^api/docs/', include('rest_framework_docs.urls')),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

